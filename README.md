# Succubus Stories

This is the documentation repo and issue tracker for the NSFW game *Succubus Stories*. The game is extremely NSFW and intended only for adults 18 years of age or older, and the documentation and issues here may contain sexually explicit depictions or language. If you are not over the age of 18 or do not wish to see such content, please leave now.

## Documentation and Guides

* [Contributing](CONTRIBUTING.html)  
* [Report Issues and Bugs](docs/issues.html)  
* [Game Manual](docs/manual.html)  
* [Changelog](CHANGELOG.html)
* [FAQ](docs/faq.html)  
* [Lists](docs/lists.html)
* [License Agreement](LICENSE.html)

## Play the Game

* [Free Version](https://outsiderartisan.itch.io/succubus-stories-free)
* [Patron Version](https://outsiderartisan.itch.io/succubus-stories-patron)

## Other Links

* [Home Page](index.html)
* [Patreon](https://www.patreon.com/outsiderartisan)
* [Discord](https://discord.gg/7BYqxYy)
