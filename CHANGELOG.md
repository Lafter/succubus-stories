## v0.14.0 (update)

> **v0.14.0** &ndash; **Date Night** &ndash; *2023-02-XX*
>
> This update brings the first part of chapter 5 to players, as well as the beginning of Charlotte's romance! And romance improvements abound with a new dating system (currently only available for Priscilla, but coming soon to everyone else). Rais hasn't been left out either with new romance content, including the long-promised ability to move in with him! Also added in this update is the brand-new fertility clinic location, a new "cock teasing" minigame, additions and enhancements to several other activities and events, and more! This update is the first part of v0.14, and v0.14.1 will be coming soon, following this up with even more dates, events, activities, and more! Stay tuned!

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/0140-datenight.png" style="margin:auto;">
</details>

### Hints and Tips

**Note:** Unmarked spoilers ahead.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>If you meet certain conditions, you can explore in the forest before moving on with the first task in chapter 5 for a special sequence of events that is miss-able, and unlocks a certain special ending in the game! It is important to do this soon, if you meet with Rais first after starting chapter 5, you'll miss the opportunity! Alternatively, because these events in the forest forever change the course of your game's story, and not necessarily for the better, you may want to *avoid* doing this event.</li>
        <li>Charlotte's romance has been added! You need to start chapter 4 for it to become available. Also, Charlotte needs to be available, obviously. You must also complete most/many Charlotte requests. These are: "Wicked Pam," "Deep Sleep," "Ass Pull," and "Sweet Relief." Fortunately all but "Ass Pull" are started by simply talking to her, while "Ass Pull" is started in the bandit camp at certain times. With all these complete, you must also engage in helping her fall asleep several times (enough to unlock everything), and have a good relationship with her. Unless you are mean to her, you should have a good enough relationship just by completing all this, but you can do the sleeping activity or give her sleeping potions to improve your relationship with her if necessary. You will get the request "The Wolf Queen" to start the relationship if you've done all this. Note that you can only choose to enter a relationship with her if you are not already in one with Rais or Priscilla.</li>
        <li>To unlock the fertility clinic sub-area, you must first complete the maid academy training, which you can do as part of the story in chapter 1. After you are able to accept maid work, and have done so a few times, you will here about a job being offered there and be able to travel there from uptown!</li>
        <li>Dates should automatically unlock once you move in with Priscilla. There is a "cooldown" between dates; the next date won't be available for several in-game time periods. The cooldown itself is random in length, but should last for around 3-6 time periods, or about 1-2 in-game days. I'm not sure if every character's dates will follow this pattern; the way I wrote some of Priscilla's dates implies you live with her, so I made that a condition instead of rewriting them. I may make this a condition for all romances' dates, or I may not, we'll see.</li>
        <li>To earn the classy qipao in the free edition, just visit the bandit camp at some point after the start of chapter 3 with high bandit rep and decent Charlotte rep. She'll give it to you as a gift.</li>
        <li>To unlock anal sex threesomes with Carol and Gray, you need to train Carol's ass a few times during one-on-one encounters with her. To unlock this training, you need to train her with her pussy a few times. So basically, [train Carol's pussy 2-3x -> train Carol's ass 2-3x -> unlock anal threesome with Carol and Gray]. For footjobs you need to train her with her feet, and you unlock that option by training her with her mouth, so it's [train Carol's mouth 2-3x -> train Carol's feet 2-3x -> unlock foojob threesome with Carol and Gray].</li>
        <li>New smaller events and where to find them: with the appropriate perks, you can make your homunculus worship your feet or ass. While exploring in the backstreets or uptown, you can randomly find a piss bucket. This leads to a similar set of actions as to what you can do in the dungeon with that bucker, but these are brand new scenes (for the most part, one part has been copied over from in the dungeon). There is a new event in the urinal alleyway's playlist.</li>
        <li>Here are some tips for the new cock teasing minigame! The "nut" meter applies a multiplier to the "pleasure" meter the higher it gets, and using the same actions too often also penalizes you by increasing the gains of said action to the "pleasure" meter, and its even worse if you use the same action multiple times in a row. To maximize your pleasure-to-nut ratio (I am so glad that phrase exists) use actions that cause a lot of please (like sucking his dick or licking his balls) early, then use lower-pleasure actions, like dirty talk and showing off repeatedly to fill up "nut" the rest of the way. When it comes to using your feet, where this action fits is random based on the partner, it always builds a decent amount of "nut" but if the patient is really into feet it'll also build a lot of pleasure! Figuring out how the patient feels about feet will dictate where in the order you want to use it&mdash;either very early or more toward the middle&mdash;so it's best to figure out early, before you start edging them. Kissing is another odd one in that it typically causes low pleasure but it's more random than the other actions, so you don't want to use it too late in case it inflicts more pleasure than you expect. I'm really happy with this activity and I can't wait to see what you all think about it!</li>
      </ul>
</details>

### Bug Fixes

- **[Interface]** Fixed issue where Priscilla spell sounds repeat in some circumstances.
- **[Other]** Various bug fixes and typo corrections.

### Content

- **[Story]** The first part of chapter 5 has been added. All story branches have been updated, and I've tried to test this as thoroughly as I can, but it's possible I've missed some combination of choices, so do let me know if you encounter any bugs or issues! We only have about 1/2 of chapter 5 left before the story is completed!!!
  - A certain other story-related event has also been added, a sequence that can unlock special endings for certain story branches, but I won't reveal how to unlock this event at this time.
- **[Requests]** One new request has been added. Start it by talking to Charlotte in the bandit camp after completing most of her other requests. This request allows you to start Charlotte's romance if you are not already in one.
- **[Romance]** Several updates here!
  - New system: Dates. You can now go on dates with Priscilla after moving in with her. This system will eventually unlock a brand new activity, and will also eventually come to all romances. Three dates have been added, with more planned in patches over the next few weeks!
  - Charlotte's romance has been added! Only the basic romance and unique encounter commentary/components have been added right now, further activities and such will be added in the future through patches over the next few weeks.
- **[Activities]** A brand new sub-area with a few small interactions and a large-scale brand new activity has been added. To unlock the new area, visit the maid academy and ask for work, after doing a few maid jobs, you will unlock the new sub-area.
  - The **Fertility Clinic** is a new sub-area. The earliest it can be unlocked is about halfway through chapter 1, after unlocking maid work at the academy. For now only one activity, detailed below, is available here, though more will be added in the future. As a quality of life improvement, talking to the doctor here will allow you to toggle fertility-related traits on or off, and also allow you to ask for inseminals when you don't have any. This should allow players to engage with pregnancy and birth content earlier in the game.
  - Engage in **Cock Teasing** at the fertility clinic, a new large-scale activity where you try to tease "patients" without making them orgasm in order to help them improve their virility. You need to balance the patients' level of pleasure with their overall level of arousal using various (mostly oral sex-related) actions. Actions diminish in their effectiveness if used too many times, so you need to vary it up as well. If the patients are pleasured too much, they'll cum, but you will have a chance to stop them with a timing-based minigame. Once they are sufficiently aroused, you can send the patient off to breed, or you can decide to double down and edge them. If you decide to edge them, the activity will start again, but be more difficult: each action will cause more pleasure than before and the timing minigame to prevent them from cumming also becomes more challenging. You can edge patients as many times as you want, and will gain a multiplier to the score and pay of the activity for each time you do so. Be careful though, if the patient cums, you get nothing (other than their cum).
- **[Art]** New outfits!!!
  - **Dragon Qipao** I promise this is the last qipao. :P
  - **Dancer's Garment** A sexy belly dancer outfit.
  - The **Classy Qipao** will become an unlockable outfit for the free edition when it is updated.


### Additions and Enhancements

- **[Advancement]** One new trait has been added: "Bandita" (as in a female bandit). It is earned by completing the new request. Having this trait causes the residents of the bandit camp to occasionally give you gifts (mostly rare ingredients).
- **[Economy]** Added additional ways to gain succubus semen without needing to engage with futanari content.
  - You can now transmute succubus semen from normal semen. It costs 2 oz of semen per oz of succubus semen. Requires the <u>basic transmutation</u> ability.
  - You can buy succubus semen from Wicked Pam's shop in increments of 1 or 5 TP.
  - Rates of collecting succubus semen on expeditions have been improved. Only witches can collect succubus semen when sent on expeditions. It was always possible to collect this ingredient in this way, I just upped the rates.
  - The homunculus now has a moderate chance to gain a fairly small amount of succubus semen when sent to gather ingredients.
- **[Encounters]** New and unique scene components and commentary have been added for Charlotte! 
- **[Activities]** Some older activities have been expanded in this update.
  - You can now engage in two new sex acts with your homunculus; having her worship your feet or worship your ass. Both require certain perks. The inverse, allowing you to do those things to her, are planned for a future update or patch.
  - Two new types of sex&mdash;anal and footjobs&mdash;have been added to threesome activities with Carol and Gray. These must be unlocked by thoroughly "training" Carol first, however.
  - New events have been added to certain playlists!
- **[Interface]** 
  - Added a new transition screen for flashbacks and dream sequences (and for entering or leaving the "otherworld") to help better ground the experience. Also, the first screen of each story chapter now has a heading displayed.
  - Added a "large portraits" setting. This allows players on larger screens to dedicate more screen real estate to the character portraits that normally only take up a tiny amount of space, but causes game text and buttons to be squished and pushed to the right, so it may not look good to everyone. Large portrait mode is optimized for 16:9 and 21:9 aspect ratios, and may give strange results on other aspect ratios
  - Optimized UI scaling and wide screen mode on ultra-wide screens. Optimized font size settings.
  - Certain requests have been renamed and designated as "Secret Storylines."
- **[Art]** Minor tweaks to some artwork.
- **[Other]** Rates for having clothing stolen by sexual partners or when bathing at the lake have been reduced.

## v0.13.3 (patch)

> **v0.13.3** &ndash; *2022-11-14*
>
> Adds a new vaginal sex actions and fixes a few critical bugs.

### Bug Fixes

- **[Other]** Bug fixes.
  - The bucket in the dungeon should now be accessible when you enter the dungeon voluntarily.
  - The silver tiara no longer errors when you put it on, and works as it should.
  - Fixed a few small typos.


### Additions and Enhancements

- **[Encounter]** Added a new action to vaginal sex encounters. Requires the "Breeding Bitch" perk to unlock.

## v0.13.2 (patch)

> **v0.13.2** &ndash; **Second Anniversary (3/3)** &ndash; *2022-11-11*
>
> Part three of the version 0.13 update! This is part three of three. The main draw in this update is PREGNANCY! Now, by using a certain potion (or, later, via a trait) the player character can become fertile, get impregnated, and ultimately give birth (to a magical orb). Also included is a new watersports activity in the dungeon, some new traits, a bit of story content, and several bug fixes and minor improvements. This patch also finally brings v0.13 to the free edition!

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/0130-second-anniversary.png" style="margin:auto;">
</details>
<details title="Click to reveal.">
	<summary>New Store Page Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/assets/img/store-b.png" style="margin:auto;">
</details>

### Bug Fixes

- **[Other]** Numerous bug fixes, corrections, and editorial changes.

### Content

- **[Story]** The dream sequence for chapter 5 has been added. Not the most impressive story update I've ever released in terms of size, but it's technically better than nothing.
- **[Art]** Three new outfits have been added for patrons! In addition to these new patron exclusive outfits, free edition users will also get a selection of outfits that must be unlocked.
  - **Classy Qipao** A lovely, classy red qipao.
  - **Sexy Qipao** A sheer, seductive qipao in purple.
  - **Bunny Suit** A classic bunny suit.

### Additions and Enhancements

- **[Alchemy]** Four new potions have been added!
  - The <u>Vaginal Inseminal</u> can be taken by the player character to make her fertile, allowing her to get pregnant when receiving vaginal creampies.
  - The <u>Anal Inseminal</u> is as above but allows the player character to become pregnant (and give birth) with her butt instead.
  - The <u>Demonic Egg &female;</u> and <u>Demonic Egg &male;</u> are what the player character ultimately gives birth to after being impregnated. These "potions" have different effects when used on the player character. Try them to see what they do. They can also be used on sexual encounter partners.
- **[Advancement]** Several new traits, and a few new perks have been added.
  - The trait line I previously said was added in v0.13.1, the "Cock Sleeve" traits, were still flagged as debug traits. This has been corrected now and the trait line should be accessible in the game.
  - Several new pregnancy and birth related traits have been added! These traits become available after the player character has given birth for the first time.
  - The new fetish "Breeding" has been added, along with two related perks. These perks enhance and change the pregnancy and birthing events.
- **[Activities]**
  - Pregnancy and birth have been added. Take an Inseminal potion to make the player character fertile, after which a creampie from the corresponding encounter type has a chance of making her pregnant. Fertility lasts until pregnancy is achieved. The chance of pregnancy occurring can be enhanced via traits. Also, the player character can unlock the ability to be fertile at all times by purchasing a specific trait, though at a much lower chance of becoming pregnant compared to the potion. After getting pregnant, the player character will experience events that progress the pregnancy until she eventually comes to term after a few in-game days, at which point she can give birth at home. With certain exhibitionism-related perks, the player can give birth in other locations as well. If the player character fails to give birth in a timely manner, she will be forced to as an emergency behavior. After the birthing event, the player character will gain a Demonic Egg item of the appropriate type.
  - A bucket has been added to the dungeon. When visiting the dungeon, the player character can interact with the bucket to perform a watersports-themed activity. There are a few different branches of this activity to discover and experience, and the player has the option to take the bucket with them when they leave, adding it to their inventory. Having the bucket doesn't do anything yet, but it will have some sort of activity or functionality in the future.
- **[Other]** Returning players (that is, players who load a save from a previous version) will receive an "Inseminal Pack" when loading an eligible save. This pack contain one of each type of inseminal.
  - In addition, Inseminals have been added as possible rewards which can be gained from certain in-game events, and to Wicked Pam's shop.

## v0.13.1 (patch)

> **v0.13.1** &ndash; **Second Anniversary (2/3)** &ndash; *2022-10-02*
>
> Part two of the version 0.13 update! This is part two of three. This patch features new cosmetics, a large new activity involving Gray and Carol (featuring some of the hottest content I think I've ever written, if I do say so myself), a new request, new traits, and quality of life and interface improvements! This is the smallest of the three parts; I cut some content due to getting COVID during production, but part three of the update (meaning v0.13.2) should be a little larger! Part three will come to free edition players at the same time as patrons, probably in mid-October.

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/0130-second-anniversary.png" style="margin:auto;">
</details>
<details title="Click to reveal.">
	<summary>New Store Page Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/assets/img/store-b.png" style="margin:auto;">
</details>

### Hints and Tips

**Note:** Unmarked spoilers ahead.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>There are two semi-random events that can happen when selling potions in the market with Carol. The first one doesn't require you to do anything special, as long as Carol is with you, there is a chance for the event to happen. Carol will give you a new recipe for "Catharsis" allowing you to craft this new potion.</li>
        <li>When offering customers blowjobs as a special offer and having Carol help when selling potions, there is a chance that Carol will approach the player character afterward to ask her for lessons on giving oral sex.</li>
        <li>At some point after the above event, when arriving at home, there is a chance Gray will visit the player and invite them over to hang out with him and Carol. This will start a request that ultimately unlocks the new activities involving Carol and Gray.</li>
        <li>After completing the above request, the player can start having sex with Carol and Gray. When both are home, the player will engage in either an oral or vaginal FFM scene with them.</li>
        <li>When only Carol is home, the player can "train" her to be better at sex in one of a variety of ways. She starts out being able to train Carol orally and vaginally, and can unlock anal and foot-oriented training sessions later. You unlock the new options by performing the existing options a few times each.</li>
        <li>When only Gray is home, the player character will perform oral sex on him to "see what her likes" to better train Carol. After a few oral sex sessions, the player will unlock the ability to seduce Gray into a special vaginal sex scene.</li>
        <li>With the new "Cock Sleeve" advanced traits, the player can now force vaginal creampies, and seduce NPCs by showing them her vagina.</li>
        <li>The "Catharsis" potion can be used on NPCs and guards to drive them off, or the player can use it on herself to trigger a "lite scat" scene. This scene isn't really scat per se, the potion makes the player leak semen from her ass when she is full. If you don't like that concept, you can just ignore the potion (or at least avoid using it on yourself!).</li>
        <li>Two new outfits have been added to the wardrobe for patrons! Check them out and try them on! I think they came out super cute &hearts;!</li>
    </ul>
</details>

### Bug Fixes

- **[Progression]** Corrected an issue preventing story progression in the prologue where certain conditions could cause the player's first potion to not count toward completing the relevant story event.
- **[Other]** Assorted bug fixes and other corrections.

### Content

- **[Requests]** One new request in this update! After seeing the player character please men when selling potions, Carol wants to learn how to better please Gray.
- **[Art]** Two new outfits have been added for patrons!
  - **Beach Babe Outfit** A sexy and summery beach-themed outfit!
  - **Witch Cosplay** A sexy witch cosplay, just in time for Halloween!

### Additions and Enhancements

- **[Advancement]** New traits have been added.
  - The new advanced trait set **Cock Sleeve** has been added, with four new traits themed around vaginal sex. To unlock this trait set, you need the "Vaginal Master" trait (the final level of the "Vaginal Expert" line) and you need to unlock the perk "Pussy Slut," which can be earned from having vaginal sex. After this, the trait "Cock Sleeve: Hot and Wet" will become available, and purchasing this trait will unlock the rest of the set.
  - A new request reward trait has been added for the new request.
- **[Alchemy]** Crafting UI improvements and a new potion!
  - Players can now craft potions in batches of 10 if they have the required ingredients.
  - When selling potions with Carol, there is a chance she will give the player the recipe for "Catharsis" potions, which can be used to sicken NPCs and guards, causing them to run away (to find a bathroom). The player character can also take these herself&hellip; 
- **[Activities]** A few new activities have been added! One, practicing sex with Carol and Gray, is sort of like three different activities in one. The other activity is the vaginal version of the "ass show" activity which can be used to seduce generic NPCs.
  - After completing the request described above, the player will be able to give Carol (and Gray, sort of) sex lessons! How this activity plays out depends on who is present when you start a "practice" session.
  - If only Gray is present, sex practice will consist of the player character sucking him off to better understand what he likes so she can then go and instruct Carol. After engaging in oral sex with Gray a certain number of times, he can be tempted into something even more intimate&hellip; Sex with Gray involves a lot of teasing and mocking, and is a pretty unique experience compared to anything else in the game!
  - If only Carol is present, the player will "train" her in various sexual techniques. These are super hot, detailed lesbian scenes where the player can choose what kind of sex to help Carol learn about. You'll start with the ability to train her oral sex skills and vaginal sex skills, but can eventually unlock additional training "courses" involving her ass and feet.
  - When both Carol and Gray are present, the player will help them have the most mind-blowing sex with each other by caressing, directing, encouraging, teasing, licking, kissing, and lubricating them. These hot threesome scenes come in two varieties, oral and vaginal sex, and end with some girl-on-girl cumplay, too! ;P
  - Added a new NPC interaction unlocked by earning one of the new **Cock Sleeve** traits. Show your pussy to NPCs to entice them.
  - Using the new "Catharsis" potion on oneself starts a new small activity. This activity will not be to everyone's taste, so feel free to ignore it. Basically, it makes the player character "release" some of the semen she has in her body&hellip;it comes out of her bum. Please note that this isn't scat, but it is kind of close to scat, maybe too close for some people. The way it works is gives you a status, and if you are too full at any time while you have that status, you will have this style of emergency and see the related scene. It does not immediately trigger the scene, you will have to explore a bit after taking the potion. The status is removed once the scene occurs.

## v0.13.0 (update)

> **v0.13.0** &ndash; **Second Anniversary (1/3)** &ndash; *2022-09-02*
>
> Part one of the version 0.13 update! This is part one of three. This update got massively out of control and way behind schedule, forcing me to break it up to get it out in a reasonable timeframe. This update includes a decent amount of stuff, I think, but nowhere near as much as I wanted to include. Still, the next few patches will round out the content here and become a great update once it's all been released!

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/0130-second-anniversary.png" style="margin:auto;">
</details>

### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>You can unlock new potions for purchase in the market by visiting Pam in the coven after having been in a combat encounter. This will lead to a request with several different endings, but regardless of how it ends you will unlock the new shop items.</li>
        <li>The above request will grant one of three different traits as a reward based on how you choose to complete it. You can complete it by: 1. talking to Bette and convincing her to leave, 2. fighting Bette and defeating her, or 3. fighting Bette and losing to her.</li>
        <li>You can unlock the new **Kissing Magic** feature any time after Carol moves in with Gray by visiting Carol at her home. Complete the request where you teach her to smooch to unlock the ability.</li>
        <li>Kissing magic starts off limited, but you earn new "spells" by doing it. Keep casting kissing magic spells on characters and NPCs to unlock new options!</li>
        <li>Visit Charlotte in the bandit camp after completing most of her other requests to unlock a new request and the ability to help her sleep. This new activity involves some gentle yuri content as you try to help her fall asleep. You can unlock new options for this activity with perks, by increasing Charlotte's affinity, and by doing the activity a few times.</li>
        <li>You can unlock the ability to "force" anal creampies via one of the new advanced "Butt Slut" traits, as well as a new activity where you can seduce NPCs by showing them your butt.</li>
        <li>You can noe complete "daily missions" for materials and G. Visit the journal menu (hetkey: E) to get started! You can complete three of these per day!</li>
        <li>Patrons can now change the player character's clothes! Three brand new cosmetic outfits are available to dress her up in! Go to the player's home and visit the wardrobe to change outfits! Outfits are unlocked automatically.</li>
    </ul>
</details>

### Bug Fixes

- **[Other]** Fixed several typos and bugs.

### Content

- **[Requests]** Added a few new requests.
  - Talk to Wicked Pam in the coven after having been in a combat encounter. You'll also need to have completed the request <u>Fixing a Hole</u> in order for this request to be available.
  - Visit Carol at Gray's house in the backstreets to help her learn some new life skills! And maybe learn a new skill yourself!
  - Talk to Charlotte after completing a certain number of other requests and unlocking the kissing magic ability and offer to help her with her insomnia! This request unlocks a sexy new activity with her and will eventually lead to her romance option in future versions.
- **[Art]** All new status and character portraits have been added to the game, replacing the old ones. This new artwork supports the new outfit system. All versions of the game will receive the updated status and portrait artwork, but only patrons will receive outfits!
  - Three new cosmetic outfits have been added to the game! Patrons may change their outfits at home in the new and improved wardrobe menu. Outfits are purely cosmetic, but they will visually change the portrait and status image of the player character.

### Additions and Enhancements

- **[Advancement]** New request reward traits, combat-related traits, and a few new advanced trait sets have been added.
  - After having been in combat one time, you will unlock the ability to invest in combat-oriented traits. The **Heal Slut** line of traits improves all healing and improves the odds of triggering the regeneration effect on Priscilla. The **Self Defense** line of traits improves the player character's kick ability, granting a chance for it to deal extra damage and improving the chances that it will stun enemies. Finally, the **Protecc** trait, which can be unlocked after purchasing **Heal Slut 2** and **Self Defense 2** grants the player a chance to deflect an attack meant for Priscilla. The odds of this happening improve based on how wounded Priscilla is.
  - The new advanced trait set **Butt Slut** has been added, with four new traits themed around, well, butt stuff. To unlock this trait set, you need the "Anal Master" trait (the final level of the "Anal Expert" line), you need to have worn the butt plug at least once (completing the request "Ass Pull" counts), and you need to unlock the perk "Ass Slut," which can be earned from having anal sex. After this, the trait "Butt Slut: Enticing Display" will become available, and purchasing this trait will unlock the rest of the set.
  - Several new request reward traits were added!
- **[Activities]**
  - Added the daily mission system. Players will receive three missions to complete each day (in real-time). These simple tasks will reward some G and a small amount of rare materials when turned in. Use the Journal (hotkey: `E`) to view and turn in daily missions. More involved or longer requests will grant improved rewards. Currently, there are no rewards for completing all three, but I may change that in the future, perhaps by rewarding TP (trait points) or or something of similar value.
  - Added a new NPC interaction unlocked by earning one of the new **Butt Slut** traits. Show your ass to NPCs to entice them.
  - Added **Kissing Magic**, a new system that allows the player to cast certain potion-like effects on NPCs by kissing them.
  - A new activity has been added to help Charlotte with her insomnia. This activity features a lot of options, and many of these options can't be unlocked unless you sleep with her a few times (3 or so) and she likes you enough, so try it out a few times!
- **[Encounters]** Added the ability to "leg lock" partners during anal sex encounters to "force" a creampie. You must unlock the relevant trait ("Butt Slut: Leglock") to perform this action. The vaginal version of this ability is coming in v0.13.1!
- **[Economy]** After completing a request added in this version, you will be able to purchase a selection of potions from the market, including some unique potions that can only be purchased.
- **[Combat]** Most health-restoring potions now have a chance to grant the "regeneration" status, which causes a small amount HP (10 to 20) to be restored passively over time. Higher quality potions have a greater chance of granting this status; the odds range from around 20% for sperm scooped off your own body to 85% for "amrita" potions. The status lasts for at least one turn, and has a growing chance of ending on every turn after the first. This should help free up players a bit in combat to try to engage in some actions other than just healing. The odds of this effect occurring are improved by the new **Heal Slut** line of traits (see above).
- **[Interface]** A number of interface changes have been made. Most of these changes are intended to improve legibility, though some are simply aesthetic in nature.
  - The background in dark mode has been darkened by a few shades to help colored dialogue text stand out a bit better.
  - Drop shadows for text have been made larger and darker in dark mode to improve text legibility.
  - The game's primary font has been changed. Some secondary fonts have also been changed. These changes are largely aesthetic, so do let me know if you don't like the new font choices.

### Performance and Technical Changes

- **[Mod Support]** The browser versions of the game now support mods via URLs. Only mods hosted on the web (or localhost) can be loaded into the browser versions of the game. In the Windows versions, mod files must be loaded from disk, and cannot be loaded remotely. In the future, the Windows version may also support remote mod files, however.
- **[Meta]** Replaced links to the game manual with links to the new [game wiki](https://outsiderartisan.miraheze.org/wiki/Main_Page).

## v0.12.3 (patch)

> **v0.12.3** &ndash; *2022-04-16*
>
> This patch mostly contains bug fixes and balance adjustments. Of note is that a few combat-oriented potions have been made much cheaper to craft, which should lessen the end-game grind.

### Bug Fixes

- **[Economy]** Rent payments were still being collected in some cases after moving into a cottage with Priscilla. This has been corrected.
- **[Interface]** The "click to reveal" feature of the content shield was bugged in certain scenarios. This has been fixed.
- **[Other]** Several other bugs and typos were corrected. Special thanks to the user Sorter on the Discord chat for catching many of these!
  - Fixed an issue in the "show off" activity that could trigger soft locks in certain scenarios.
  - Fixed an error that occasionally caused a certain event in the lake to be bugged and soft lock users.
  - Added missing data for a sex action to the locked actions list.

### Additions and Enhancements

- **[Social]** Added more content to the chit chat feature.

### Progression and Balance Changes

- **[Alchemy]** Reduced the cost of certain potions to make combat less grindy.
  - <u>Heat Liquors</u> no longer cost and gem dust, and other required ingredients have been reduced. 
  - <u>Heat Liquors +</u> no longer cost any ale, and other required ingredients have been reduced.
  - <u>Preserved Sperms</u> cost a little less semen. 
- **[Encounter]** Reduced the perk requirement needed to use <u>Lacticals</u> during lesbian encounters. (Switched from requiring <u>Lady Lover</u> to requiring <u>Bi the Way</u>.)

## v0.12.2 (patch)

> **v0.12.2** &ndash; *2022-04-11*
>
> This patch brings the **Love and War** update to the free edition of the game, fixes a few bugs, adjusts the game balance, and adds some new features!
> 
> Of particular note, players may now use potions during encounters. See below for a list of potions that can be used and their effects. You can press the `P` hotkey during encounters to quickly access the potion list. New traits relating to using potions in encounters have also been added.
>
> And finally, mod support has finally arrived in the free edition of the game. [Mod tools and documentation are available here](https://outsiderartisan.neocities.org/succubus-stories/modding/#/). Two official test mods previously released to patrons to test the mod systems are also now available for free users to download and enjoy!

### Mod News and Updates

> Mod support is now included in the free edition of the game!

- **[Mod Tools]** Mod tools have been released in beta! You can get them, along with guides, documentation, and examples, [here](https://outsiderartisan.neocities.org/succubus-stories/modding/index.html#/docs).
- **[Mod Release]** Convent and the Sniffing mod are now available for free users and can be downloaded via [Itch.io](https://outsiderartisan.itch.io/succubus-stories-free).

### Bug Fixes

- **[Advancement]** The trait <u>Nymph: Always Horny</u> has been renamed to <u>Nymph: Zen</u> and its description has been adjusted slightly. The trait's effect is unchanged.
- **[Interface]** Certain hotkeys did not work when `Caps Lock` was on; this has been corrected.
- **[Other]** Fixed some bugs.
  - Corrected an issue where players could skip paying rent by searching for more customers during prostitution.
  - Corrected an issue where masturbating in certain situations was not correctly lowering player lust.
  - Fixed a few typos and grammar mistakes and made some editorial changes.


### Additions and Enhancements

- **[Advancement]** Some new traits related to using potions during encounters (see below) have been added. These traits boost the number of potions you can administer in a single encounter and reduce the penalty to performance for using potions.
- **[Encounters]** You can now use some potions during encounters to trigger unique effects. You can only feed your partner a few potions before they start refusing to take them. Using any potions at all has a small negative effect on performance.
  - You can use <u>Piss Liquors</u> on partners, prompting them to urinate. This allows you to collect male/female urine, as well as playing unique encounter scenes. Requires perks relevant to watersports to unlock.
  - You can use <u>Lacticals</u> on female partners. Doing so allows you to breastfeed from her as an action, and may change some scene descriptions to account for this effect. If the partner is already lactating, this option will not be available. Female partners have a very small chance of already being able to lactate from the start of an encounter. Requires perks relevant to lesbianism in order to unlock.
  - Finally, you may use <u>Heat Liquors</u> and <u>Heat Liquor +</u>&apos;s on partners, increasing their lust and carrying a chance to restore some of their energy. For male partners, their semen production will also be improved.
- **[Interface]** Added the `P` hotkey to encounters to open the potions modal.

### Balance and Progression Adjustments

- **[Needs]** Several adjustments to lust build-up.
  - Adjusted the curve of player lust build-up over time. Players should now gain noticeably less lust over time when lust is low. Lust build-up over time will increase by a slightly larger amount as player lust increases. The overall effect is that lust should remain at lower levels for longer, but only take a few more time periods overall to reach emergency levels.
  - Shame, embarrassment, public nudity, having had recent anal sex, and wearing the butt plug all now increase lust less over time, by about half for each effect. Even with the adjustments to the lust curve, these effects made managing lust a bit too difficult when combined, especially because the butt plug and recent anal sex effects will trigger when moving locations even if no in-game time has passed.

## v0.12.1 (patch)

> **v0.12.1** &ndash; *2022-04-05*
>
> Hotfix for a dead end in the "ashlands" dangerous area.

### Bug Fixes

- **[Other]** Fixed dead end that can occur toward the end of the "ashlands" dangerous area.

## v0.12.0 (update)

> **v0.12.0** &ndash; **Love and War** &ndash; *2022-04-04*
>
> There's now no choice but to fight. As Rachel's machinations bring the kingdom ever closer to collapse, the player character will be forced to take up arms alongside Priscilla in order to stop her. This update features the conclusion to chapter 4, with a large amount of brand-new story content alongside new combat and dungeon-crawling game modes. Explore and fight through the story like never before.
> 
> But that's not all! Romance is in the air! Rais's romance path has been added to the game, featuring tons of steamy new content to experience. Fun and sexy expansions have also been made to Priscilla's romance path. Also included in this update are new activities, new requests, new and expanded side content, new character progression options, tons of new artwork, and much more! 

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/0120-loveandwar.png" style="margin:auto;">
</details>

### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>There are several different story paths in this update, with completely separate events, dangerous areas, and combat encounters. The content you experience in this update is based on the choice you made way back at the end of chapter three. There are six main paths, but there's significant overlap between many of them such that there are more like four main paths through this part of chapter 4. What you do in this part of chapter 4 will also be incredibly important to which ending you end up setting yourself up for in chapter 5.</li>
        <li>Visiting Rais at certain points throughout the game will cause several different small events to play out. After seeing a few of them, you will unlock the <strong>Sneaky Sneaky</strong> request.</li>
        <li>After completing the <strong>Sneaky Sneaky</strong> request and performing the under desk activity at least one time, if Rais likes you enough and you are in good standing with the guards, the quest to romance him will appear early in chapter 4. You can perform the desk activity as many times as you need to improve your relationship with him if needed, but it's possible to have a high enough rep with him without needing to do it more than the one required time.</li>
        <li>You can now move in with <strike>your romantic partners</strike> Priscilla! The option to do so appears after you've been with them for a certain amount of time and have improved your relationship with them to a certain point. Agreeing to move in with your lover will provide you with rent-free housing! (Currently this is only implemented with Priscilla. Sorry about that! Rais's move in option will be coming soon!)</li>
        <li>There are two new activities for Priscilla and Rais: helping Priscilla feed (in a sexy way) and sucking Rais off under his desk while he's at work. You can do either of these activities without needing to be in a romance with the related character.</li>
        <li>You can help Priscilla feed after meeting certain conditions. If you are not in a romance with Priscilla, you need to have the perks <strong>Cum Slut</strong> and <strong>Bi the Way</strong>, and have a good relationship with her to see the option for this activity. You can help Priscilla feed in one of three different ways. You can store an anal creampie by wearing the butt plug and having anal sex, which you can then feed to her. If you are at a certain level of filthiness, you can feed her from the cum on your body. Finally, you can take Priscilla with you and have sex with someone. Priscilla will observe the sex scene and comment on it/react to it. After the sex scene, the player will share the "food" with Priscilla.</li>
        <li>Tiaras are a new type of item added to the game in this update. Tiaras provide powerful, game-changing effects but only one can be worn at a time, and the conditions to unlock some of them may take some time to unlock. Once you meet the conditions to unlock a tiara, it will be automatically delivered to your home for you to find the next time you return there.</li>
        <li>Finally, here are a few combat tips. The <strong>Kick</strong> action can cause enemies to become <em>staggered</em> making Priscilla's next attack deal extra damage. <u>Semen Pearls</u> can be used to restore both HP and MP to Priscilla. If you're lactating, you can use your breast milk on either combatant. It will heal HP and restore MP for Priscilla at the cost of a lot of lust, while for enemies it will raise their lust and inflict the <em>drowsy</em> status. Remember that you can leave and re-enter dangerous areas at any time and <em>most</em> progress will carry over!</li>
    </ul>
</details>

### Mod News and Updates

> Mod support will be released to free edition players as of v0.12!
> 
> On top of that, mod tools and docs have been released in beta for all players!

- **[Mod Tools]** Mod tools have been released in beta! You can get them, along with guides, documentation, and examples, [here](https://outsiderartisan.neocities.org/succubus-stories/modding/index.html#/docs).
- **[Convent]** The Convent official game mod is now available to free users! Some small updates have been made to Convent, mosty just minor content additions and fixes. Nothing worth enumerating.
- **[Sniffing (Oral Sex Action)]** The sniffing action official game mod is now available to free users!

### Bug Fixes

- **[Activities]** Fixed issue where NPC options were being presented for the incorrect genders in certain situations.
- **[Social]** Fixed several bugs with the chit chat feature, including the non-functional bandit camp chit chat activity.
- **[Other]** Fixed miscellaneous typos and bugs. Made some editorial and content changes to various scenes and events.

### Content

- **[Story]** The rest of chapter 4 has been added to the game! This represents the largest chunk of story content ever added to the game in a single update. However, due to the story's branching nature, it might not feel like it to play through. Then again, with a bit of exploration and combat thrown in, it may balance out. I hope it feels just right!
- **[Requests]** Two new requests in this update.
  - Visit Rais's office a few times and the player character may get the idea to play a prank on him&hellip;
  - After the player character's relationship with Rais reaches a certain point, he has something he wants to tell her. This request starts Rias's romance path, and if you choose to accept his feelings, you will not be able to be in any other romances.
- **[Activities]** This update features a few new activities.
  - As part of the new story content, the player character will pair up with Priscilla to explore **dangerous areas**. These are sort of like dungeon crawls; they feature light puzzle-solving and exploration elements and dangerous enemies who will attack the two succubi. There are a total of three of these zones in the game, though most paths will only be able to explore one. You can only spend a certain amount of time in dangerous areas before being forced to leave, and being defeated in combat will also force you to retreat. You may also choose to leave of your own accord. You can freely re-enter the dangerous zone whenever you want, though, and progression toward your goals carries over, so it shouldn't be possible to get stuck even if you find these areas hard.
  - The dangerous areas mentioned above feature **combat**, a brand new gameplay system where you support Priscilla while she fights enemies. Priscilla will act of her own accord, but the player will need to manage her HP, MP, and lust by using potions and various actions to keep her in the fight. You can also use potions and perform actions on enemies. Combat isn't intended to be a major mechanic of the game, more just an interesting gameplay system for players to interact with. If players feel it's fun, I may expand the system to add more options and content. If it's not well-loved, I'll try to make it as fun as I can, but won't expand it beyond the few story appearances it's intended to have here, plus a few additional instances in chapter 5. Be warned that combat balance is a work in progress.
  - After completing a request, you can help spice up Rais's workday by hanging out under his desk and stealthily sucking him off while he's working (and having various meetings). You'll want to try to balance getting him off with not stressing him out by remaining somewhat quiet when others are present, and going to town on him when you're alone. You will not need to be in a romance with Rais to participate in this activity.
  - If you meet certain requirements, you can feed Priscilla in a number of fun and sexy ways. You will not need to be in a romance with Priscilla to participate in this activity.
  - Finally, Rais's romance has been added in this update. You will only be able to be in one romance at a time, so you can't romance him if you're already with Priscilla. You can start his romance anytime after starting chapter 4 and before a certain point in chapter 5 (which is not yet in the game yet). To start his romance, you must be past a certain point in the story, have a decent relationship with the guard faction, complete the request **Sneaky Sneaky**, have performed the desk activity at least once, and have a high reputation with Rais personally (you can do the desk activity mentioned above for easy rep, if you want). After all these conditions are met, you will receive the request where you can choose to accept or reject Rais's feelings. Accepting Rais's feelings and starting his romance will lock you out of all other potential romance options for the rest of the playthrough.
- **[Art]** Tons of new artwork has been added!
  - Rais now has portraits for both art sets!
  - New rendered CGs for anal and vaginal sex with Rais have been added.
  - A whopping **24** new hand-drawn CGs have been added.

### Additions and Enhancements

- **[Advancement]** Two new special traits can be earned by completing the new requests. These are fairly minor, but I think should at least be useful.
- **[Activities]**  Some activities have been expanded.
  - Priscilla's existing romance has been upgraded and improved to be more in line with Rais's new romance. These new romance features include improved sex activities, small events, additional dialogue, and more.
  - You now have the ability to move in with your lover (which will net you rent-free housing, if you don't already have it). <u>Note</u>: This feature is only available for Priscilla's romance as of v0.12.0; it will come to Rais's romance in a future update.
  - Additionally, small updates were made to some existing activities, mostly in adding new content.
- **[Encounters]** No new actions or encounter types were added in this version, but some new scene components were added alongside some revisions and improvements to existing scene components.
- **[Interface]** Buying traits is now a much smoother experience, and no longer closes the trait menu to show a confirmation; there is still a confirmation, but you just click on the same trait again to confirm the purchase. I think this is a vast improvement.
- **[Requests]** Most requests that can no longer be completed due to story progression are now removed from the in-game journal. Unfortunately, there is really no way to warn players when story progress will cancel a request in the current system as I've designed it, but there is really only one main story event that cancels most requests right now: <span title="Major Spoiler!" style="filter:blur(4px);cursor:pointer;" onclick='this.style.filter = ""'>Priscilla moving to the bandit camp. This is due to Priscilla's motivations; most of these canceled quests involve the player helping her do something at the behest of her former mistress, so these quests would make no sense after Priscilla moves.</span> This event happens in chapter 3; if you want to complete all of Priscilla's content, do so before moving on past the very beginning of chapter 3. Note that at least one request you pick up in the barracks is tied to Priscilla as well, and gates some barracks content too.
- **[Other]** Players who meet certain conditions may begin to receive strange gifts from a mysterious benefactor&hellip;

### Balance and Progression Adjustments

- **[Alchemy]** Minor tweaks to potion prices and recipes.
- **[Advancement]** The amount of experience required to earn a trait has been reduced by approximately 20%. If you still feel progression for traits is too slow or grindy, there are traits that increase trait XP earned from various activities, so I suggest purchasing these traits as early as you can. They may seem like a waste of traits, but they are actually quite potent, and should pay for themselves relatively quickly.
- **[Social]** Minor changes here, mostly to make the first two chapters feel a little faster. There's a lot more story and content to unlock in the game now, and chapter 1 feels like it drags a little bit.
  - Reduced requirements for some social skill checks, mostly in the story, like entering Wimbol's manor for the first time.
  - Reduced reputation requirements for certain faction and character unlocks (mostly related to unlocking requests).

### Performance and Technical Changes

- **[Executable]** Some internal improvements have been made to mod support. Most types of modded content will now be flagged internally by the engine to prevent certain types of mods from making the game unstable or messing with game systems outside of modders' control. For example, modded perks will be ignored by the "recommended activities" system, and modded actions will not appear in the "locked actions" list during encounters.
- **[Web]** Performance optimizations and other minor improvements. As the game gets bigger, these sorts of tweaks get harder and harder to make. Fortunately, I believe most systems at most download speeds should still have a reasonably good time.
- **[Misc]** The game console, which was previously only accessible in the patron edition when cheats were enabled, is now always accessible. This change is in order to facilitate modding. Users are encouraged to use the console with care, as it is possible to create bugs and other problems, up to and including soft-locks and even game crashes.

## v0.11.3 (patch)

> **v0.11.3** &ndash; *2021-12-25* (Merry Christmas!)
>
> Fixes a few bugs and spelling errors. Mod support can be unlocked in the browser edition, if run in NW.js (this was added by user request, and is not recommended; only Windows officially supports mods).

### Bug Fixes

- **[Other]** Fixes a few bugs and typos.
  - Starting the flashing activity via futa potions was causing a loop; this is corrected.
  - Several typos fixed.

### Performance and Technical Changes

- **[Web]** The web version will now detect if it is being run in NW.js. If it detects this browser shell, it will allow mods to be loaded. Note that mods are still only *officially* supported in the Windows executable releases for now.

## v0.11.2 (patch)

> **v0.11.2** &ndash; *2021-12-21*
>
> Hotfix for mod support issues. Only released for patron edition executables.

### Bug Fixes

- **[Mod Support]** Fixed bug causing modded perks to display errors in certain scenarios.

## v0.11.1 (patch)

> **v0.11.1** &ndash; *2021-12-20*
>
> This update brings the content from the **Selfcare** update (v0.11) to the free edition of the game. Also added to the free edition is the new *hand-drawn art set* by the talented [Oni-san](https://twitter.com/oni_saaan).
>
> This patch also adds in some features and content I didn't quite manage to finish in time for the release of v0.11, and many, many bug fixes, so patrons may want to check it out, too!

### Bug Fixes

- **[Interface]** The status indicators are now properly removed on small screen and window sizes. This feature was previously bugged.
- **[Other]** Bug fixes, editorial corrections, typos, and minor content changes throughout the game.
  - Fixes to several areas where the new art set was messed up or not displaying correctly.
  - Dozens of typos and grammar errors fixed.
  - Corrections to several scenes and events, mostly in the first half of the story and in several early requests.
  - Editorial changes to many in-game tutorials to make them shorter and easier to understand.

### Content

- **[Other]** Some random events have been added. Nothing major or worth seeking out, I don't think, but you may notice some additions.

### Additions and Enhancements

- **[Advancement]** New perks: <u>Wild Jacker</u> and <u>Meat Toilet</u>. The former adds an action to the futanari masturbation activity (depending on the time of day and location), and the latter will add several actions in a future update.
- **[Encounters]** This patch adds the "Locked Actions" button to the encounter interface. A small lock icon will appear to the left of your encounter options. You can click this icon to see a list of options you have yet to unlock for that encounter type. Clicking on an action in this list will tell you what perks/traits/statuses/etc you need and give you recommendations on what kind of activities and encounters to seek out. Note that actions added by mods will not appear, only base-game actions.
- **[Interface]** The to-do list has been overhauled. Well, more accurately, I finished overhauling it. You can now see the to-do list at your home after reaching chapter 2. This is explained in the story by Rais giving you a notebook to sort out your thoughts. For players who already passed that part of the story, you just get it when you load your save; you're not missing anything too crazy story-wise. The to-do list will still appear after reaching the end of the currently available story content as well.

## v0.11.0 (update)

> **v0.11.0** &ndash; **Selfcare** &mdash; *2021-12-15*
>
> This update comes with a ton of new ways to play...with yourself! New and expanded masturbation content with both normal and futanari varieties have been added, along with tons of related perks, traits, and abilities. Masturbation as an activity, when performed outdoors or in public, or in front of NPCs with a new generic NPC action, now plays out like an encounter! Also added in this update is a wearable butt plug, which has an effect on a variety of game systems and activities.
>
> While no new main story content has been added in this update, two new requests have been added, one of which is extremely large and involved. New and improved activities and events have been added, as well as new potions, character advancement options, and more.

<p><details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/0110-selfcare.png" style="margin:auto;">
</details>
<details title="Click to reveal.">
	<summary>New Store Page Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/shelf/final.png" style="margin:auto;">
</details>
<details title="Click to reveal.">
	<summary>New Banners (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/banners/final-patreon.png" style="margin:auto;">
	<br><br>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/banners/final-21x9.png" style="margin:auto;">
	<br><br>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/banners/final-tall-alt.png" style="margin:auto;">
</details>

### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>With the <strong>No Shame</strong> perk, you can now engage in a brand new masturbation activity when outdoors, similar to an encounter. There is also a futanari variant. You will unlock new actions for this activity with perks and statuses, just like sexual encounters.</li>
        <li>Using the <u>futa potion</u> while you have the <strong>No Shame</strong> perk will allow you to masturbate in areas you normally couldn't, and you will gain unique options on where you want to make messes with your cum if you do so.</li>
        <li>Prior to earning the <strong>No Shame</strong> perk, using a <u>futa potion</u> will either not work, or, if you have the right perks and are in the right kind of place, doing so will start the flashing activity.</li>
        <li>Visit the bandit camp in the morning and help Charlotte with a request to unlock the butt plug.</li>
        <li>The butt plug can be equipped at the player's home and worn around. Wearing the butt plug has a variety of effects. While equipped, the player character's lust will grow at a faster rate. With the right perks, the player character can use it as part of an action in lesbian scenes. It will enhance the chance of critical pleasure during all sexual encounters, but especially during vaginal sex. When the player character takes an anal creampie, from an anal encounter, in the stocks, or esle where, the butt plug will trap the semen in the player character's ass until removed. Being full of semen in this way also has other effects, like enhancing a partner's pleasure during anal sex.</li>
        <li>When you have the <strong>No Shame</strong> perk, you can transition directly into a masturbation activity from a flashing activity if you choose the masturbation ending.</li>
        <li>The new <strong>Selfish</strong> trait set (see below for details on how to unlock) allows the player character to "store" orgasms by masturbating, which she can then use in encounters or in other ways. With an additional trait, she can use this power to conjure a penis instead of using a <u>futa potion</u>.</li>
        <li>With the appropriate trait, the player character can "show" and NPC how she masturbates, basically masturbating in front of a single person, who generally reacts with disgust. Certain NPCs will add additional scenes and options to masturbation or futanari masturbation when action this is used on them.</li>
        <li>If you need more tips or want to find out what you might be missing, ask on <a href="https://discord.gg/7BYqxYy" target="_blank">the discord server</a>.</li>
    </ul>
</details>

### Mod News and Updates

> The game mod [*Succubus Stories: Convent*](https://outsiderartisan.neocities.org/succubus-stories/modding/convent.html) was released on December 2nd for patrons only, and features a new sub-area and faction focused around freeuse/public use/submissive content. It includes new activities, both in the new sub-area and elsewhere, new perks, new traits, and other new and sexy stuff to discover.
>
> This is an official mod, meaning it was made my me. It's also a test mod, meaning it's designed to help test mod support and may be buggy or have issues. You'd be doing me a huge favor if you downloaded it and the other test mod(s) over on [the Patron Edition's Itch.io page](https://outsiderartisan.itch.io/succubus-stories-patron/patreon-access) and gave it a shot. Please post on [the Discord server](https://discord.gg/7BYqxYy) with any issues.
>
> Thanks!

- **[Convent]** Some adjustments have been made to this mod.
  - The rate at which you unlock the new perks has been slowed by a significant amount. While I know no one likes a grind, it was a bit *too* fast, almost to an absurd or unmanageable degree.
  - The rate of unavoidable freeuse events that occur upon entering the convent with the appropriate traits has been reduced significantly (18%/36% &rarr; 11%/22%).

### Bug Fixes

- **[Progression]** Fixed some bugs related to trait requirements.
- **[Alchemy]** Fixed issue where some potions were not properly removed from the potion inventory after being used.
- **[Other]** Fixed miscellaneous typos and bugs.
  - Fixed an issues with the hand-drawn art set's status images not always showing up.
  - The "No Art" setting now removes status images from the character menu as well. Status indicators are not currently affected, but may be in a future update.
  - Fixes for hotkeys on some systems.

### Content

- **[Story]** There is **no new story content** in this update. :'(
- **[Requests]** Two new requests in this update.
  - Visit the bandit camp in the morning to find Charlotte planning a risky job with one of her bandits; a job the player character may be uniquely qualified to help with. You'll need to reach a certain point in the story for this request to be available. This is a long and involved request, so make sure you're ready for it; you won't be able to return to the sandbox for a while after reaching a certain point in it.
  - A woman in the market is desperate for a legendary potion to cure her ailing father, a potion only the most skilled alchemists could hope to make.
- **[Activities]** This update features several new and expanded activities and features.
  - A new <u>chit chat</u> option has been added all across the game world, allowing you to interact with a variety of NPCs in a variety of locations. NPCs can be chatted with for lore, or you can perform various general actions on them, like administering potions. It should now be very easy for you to find NPCs to try stuff on without having to track them down in random events. [This post from the Patreon](https://www.patreon.com/posts/58926120) goes into more detail about how chatting works. The system is in its infancy; future updates will add more character types, conversation options, and actions.
  - Masturbation has been overhauled dramatically. Private masturbation has not been changed, and the old, single-screen masturbation scenes are still available as an option called "Quick Masturbation," however, normal masturbation in public is now a complete activity similar in scope and feel to an encounter. Normal, non-futanari public/outdoor masturbation can be done at the same locations as before: the backstreets and uptown. If people are around they will react. Men from the surrounding area may also get involved in the fun. [This Patreon post](https://www.patreon.com/posts/59029537) explains the new masturbation mechanics in more detail.
  - Futanari masturbation has been similarly overhauled, but features completely different scenes, options, and NPC reactions compared to normal masturbation. Additionally, you can masturbate in more places as a futanari thanks to the <u>futa potion</u> (more below). The player character can also choose what to do with her semen when she ejaculates; each location you can masturbate in has unique options for where you can shoot your loads! Some other factors, like perks and the time of day, also have an effect on these options. [This Patreon post](https://www.patreon.com/posts/59029537) explains the new masturbation mechanics in more detail.
  - With the appropriate perks, you can now also "show" an NPC how you masturbate, meaning you masturbate in front of that specific NPC. The normal public reactions are replaced by that specific NPC's reaction (usually of confusion or disgust). The reactions are mostly just random for now, rather than according to the type of NPC, but I do want to make it a bit deeper in a future update. Also, certain NPCs may provide you with new options for masturbating and cumming, too so look out for that.
  - Finally, you can now get a butt plug in the game as a request reward. Generally the butt plug enhances the chance of the player character experiencing critical pleasure during encounters, *especially* during vaginal sex. Butt plugs also have a major effect on lesbian encounters, with the correct perks. Having a butt plug equipped causes lust to build up at a greatly elevated rate. She can also "lubricate" it every so often as an action anywhere she is (she uses her mouth to do this). It doesn't cost any time, but once it's lubricated, you need to wait a bit before you can do it again. The butt plug also causes anal creampies to persist, which also has several potential effects. I think it's actually a fairly deep system here.
- **[Art]** The new hand-drawn art set will be coming to the free edition in this update! Technically the free edition will be getting the art later, but the code to give it to them is already in this version. Other artistic changes to the game have also been made, you can read about those below in the **[Interface]** entry in the **Additions and Enhancements** section.

### Additions and Enhancements

- **[Alchemy]** One new potion and several changes to the way <u>futa potions</u> work.
  - <u>Amrita</u> is a powerful and rare medicine that can be made only from advanced alchemical ingredients. Even demons benefit from <u>amrita</u>; if the player drinks amrita, it will sate both her hunger and lust for a long period of time. <u>Amrita</u> can be sold for a hefty price as well. You gain this recipe via a request.
  - The <u>futa potion</u> has been completely reworked. It's a little complicated now, but the long and short of it is that if you have some exhibitionism perks, taking a <u>futa potion</u> in public will start a flashing activity. If you have the ability to masturbate in public, taking a <u>futa potion</u> will instead start the futanari version of the public masturbation event. You can use this to masturbate in places that don't otherwise give you that option, like the market or the coven, and doing so will give you unique opportunities for where to shoot your loads. [This Patreon post](https://www.patreon.com/posts/59029537) explains everything in greater detail.

- **[Activities]**  Two small updates to existing activities, and other activities have been "bridged" together.
  - When engaging in prostitution with a female customer, you will now have the option to use <u>futa potions</u> to engage in futanari sex instead. You must have the <u>Bold Flasher</u> perk if many people are around in order to have public futanari sex.
  - When you sedate an NPC with your breast milk or a <u>sleeping potion</u>, you can now choose to steal their under garments.
  - There are now "bridges" between certain existing similar activities. For example, choosing to masturbate after the flashing activity now carries directly over into the new masturbation expansion if you have the requisite perk (<u>No Shame</u>) rather than playing the normal masturbation ending. The normal masturbation ending will still happen prior to you unlocking the required perk. The new masturbation activity can in turn lead to the player character being propositioned by an onlooker, which can lead to even more content.
- **[Encounters]** A new action was added, usable in every encounter type. Also, encounters have been updates in several ways to reflect the presence of butt plugs where appropriate. 
  - Butt plugs are now accounted for in encounters and will be mentioned when relevant. When the player experiences critical pleasure due to the butt plug, it will be mentioned in the scene description.
  - When having lesbian sex, with the <u>Ass Slut</u> perk and with an equipped butt plug, you will have the option to use the butt plug on your partner as part of a sex act.
  - You can now unlock the ability to masturbate during a sexual encounter, essentially ignoring your partner. Doing so grants the player character a decent amount of pleasure, and the partner very little, if any.
- **[Advancement]** Lots of new traits in this update. New advancement options and perks as well.
  - A new trait line has been added. It adds various temporary "afterglow" effects that last for a short time after the player character masturbates. The traits are: <u>Self Sufficiency</u>, <u>Self Actualization</u>, <u>Self Satisfaction</u>, <u>Self Realization</u>, and <u>Cult of Self</u>. I won't explain each one, but you'll have to buy them all and the <u>Metabolism</u> line, as well as earn a few new perks to unlock the advanced <u>Selfish</u> trait set (see below).
  - A new advanced trait set, the <u>Selfish</u> set, has been added. To unlock it, you need the perks <u>No Shame</u> and <u>Self Love &hearts;</u> and the trait  <u>Self Satisfaction</u>. After purchasing the first trait in the set, you can purchase the rest in any order.
    - <u>Selfish: Stored Pleasure</u> - Masturbating allows the player character to store some of the pleasure for later. The player character can use this pleasure to make herself instantly cum during encounters. She can store up to 3 orgasms this way.
    - <u>Selfish: Showoff</u> - This trait grants the player the ability to masturbate in front of an NPC and a generic interaction. See above for more.
    - <u>Selfish: Mystic Manhood</u> - Allows the player character to spend one of her stored orgasms from <u>Selfish: Stored Pleasure</u> to conjure a penis and become a futanari in lieu of using a <u>futa potion</u>. There may be some instances I missed where the player can use a <u>futa potion</u>, so if you find any places where you can use the potion but not this ability, please let me know.
    - <u>Selfish: Full Pleasure</u> - Allows the player character to store up to 5 orgasms with <u>Selfish: Stored Pleasure</u>. Additionally, each stored orgasm reduces the rate at which she grows hungry, with a stock of 5 preventing her from getting hungrier at all due to the passage of time.
  - A new sexual experience type, <u>masturbation</u>, has been added. A new fetish, <u>onanism</u>, has as well. For now, they only have two associated perks. See below.
  - Two new perks have been added. <u>Self Love &hearts;</u> allows the player to masturbate during a sexual encounter (see above). <u>Mad Masturbator</u> makes masturbating during encounters count as a favored action, raising the chance of it scoring critical pleasure on the player.
- **[Interface]** Some aesthetic changes have been made. Some status indicators have also been added.
  - This update features some aesthetic changes to both the normal and dark themes, some improvements to readability to the dark mode, and some other tweaks and changes. I think the game is actually dramatically improved visually by these changes, but you can let me know your opinion on the Discord server or by sending me a message on Patreon. If people don't like the changes, we can revert some or all of them.
  - Indicators for character nudity and whether they are wearing the butt plug have been added to the UI. These will appear in the top right. On small screens, and during encounters, these indicators will not appear.
  - The to-do list on the **To Be Continued** screen has been dramatically overhauled, and more changes are planned for the future.
    - You can now review unlocked perks and traits from here, to see what you still have to unlock. Note that some request traits are alternate rewards for completing requests in different ways, and so may become impossible to earn.
    -  You can review activities/events and requests to see which ones you've found/completed. Some of the listed activities or events may be special endings to other activities and events requiring specific circumstances to complete.
    - All story-related options have been removed, as these cannot generally be done after they are missed. More than thirty items were removed from the list, but my feeling is that it's still too long; the list may need further sub-divisions or just trimmed way down. This means everything on the list should now be possible to complete for all players.
    - The activities and request lists feature buttons you can use to hide all the completed entries for easier browsing. I may add this feature to the other lists as well.
- **[Other]** The home menu no longer has Dress and Undress actions, instead you go to a separate wardrobe screen where you can dress or undress, or equip the butt plug if you have it. More options for stuff like other sex toys will be coming in future updates. Probably no actual outfits though.

### Balance and Progression Adjustments

- **[Advancement]** Some progression and balance adjustments were made.
  - All of the metabolism traits were nerfed so that the new traits could effect hunger without being redundant. It's now possible to freeze hunger basically indefinitely with the new traits, but with the metabolism traits as good as they were, you'd never need to. 
  - Some other traits also got minor rebalances, mostly if the overlapped with these new traits to prevent game-breaking combinations.
  - The requirements for advanced trait sets are now much less strict.
  - Some adjustments were made to public sex and prostitution as it relates to perks. Players can now engage in vaginal and anal sex during prostitution in the backstreets and uptown once the <u>Bold Flasher</u> perk is unlocked, and even before then as long as it is night time. The player character will take the customer to a less populated place. Earning further perks allow them to have sex in front of more and more people.
  - Urinating at home or in the dungeon (when done "normally") no longer counts as a watersports activity.
- **[Encounters]** Adjustments to critical pleasure and some other minor changes.
  - Critical pleasure is now rarer by default for the player character, but the butt plug raises the odds past where they were before. This is another instance of preventing game-breaking or game-ruining combinations of effects. Note that equipping the butt plug may make vaginal sex scenes hard to manage.
  - Special sexual encounter options (orgasmic suggestion, releasing stored pleasure, etc) now appear at the bottom of the action list rather than the top. This should make the action lists and hotkeys feel more consistent.


### Performance and Technical Changes

- **[Executable]** Changes for mod support and better integration with the [Itch.io App](https://itch.io/app).
  - Several updates were made to mod support, specifically to give modders a way to run global scripts. Convent needed this feature, and while I built around that need, no one else should have to. 
  - Improved Itch.io App integration.
- **[Web]** Preloading images in the web version is a little slower now, but should work more reliably. Only lower resolution assets are preloaded.
  - By default, hand-drawn art set images in the web version will default to lower resolution versions (intended to look good on 1080p screens). For users with fast Internet connections or 4K monitors, you may wish to opt for high resolution images from the settings menu. The executable build will always use the high resolution assets. Renders are not affected by this change, and only have one resolution available.  
- **[Other]** A few tweaks, improvements, and internal changes were made to the engine in this update.
  - The console (on the patron edition with cheats on, hit `~`) now has expanded features and can now execute some code. It executes this code in the engine's scope, and should make testing mods easier in the future. The console may not remain patron-exclusive after mod support comes to the free edition due to its utility for testing mods.
  - Totally rewrote all the code that deals with hotkeys. It should now be more efficient and work on more systems and browsers and keyboard types. Also added `[` `]` and `\` as three additional hotkeys for screens with tons of options. That brings the total number of hotkey-able options to 15, which is *just barely* enough for everything in the game right now. I don't want to be limited by this, but fifteen options is a lot, probably too many for one screen, so I want to resist making even more hotkeys and focus on splitting options off or maybe grouping them together. Like maybe urination, lactation, and masturbation can all go inside a "needs" submenu to free up the main area screens.

## v0.10.1 (patch)

> **v0.10.1** &ndash; *2021-11-05*
>
> This patch brings the **Anniversary** update to the free edition of the game! 

### Bug Fixes

- **[Activities]** The new maid work activity was not implemented correctly and was not appearing in the playlist in v0.10.0, but this has been fixed and it should appear correctly now!
- **[Social]** Certain NPCs didn't present the player with generic options when they should have. This has been rectified.
- **[Other]** Fixed several typos! Thanks to patrons and players for reporting all of these, especially over on the Discord server!

### Content

- **[Art]** One new rendered CG was added. I had this one floating around a while and wanted to put it in v0.10.0, but forgot, so here it is!

### Additions and Enhancements

- **[Cheats]** Added an option in the cheat menu (accessible from the player character's home when cheats are enabled via the game setting) for giving yourself trait points (*patron edition only*).
- **[Interface]** Small (very small) tweaks.
  - Renamed the "Log" to "Journal."
  - Removed the link to outdated documentation on the main menu and replaced it with a link to the game's Discord server.
  - Adjusted tab positioning in some game menu modals.
- **[Onboarding]** Reframed and expanded some early game tutorials.

## v0.10.0 (update)

> **v0.10.0** &ndash; **One Year Anniversary** &ndash; *2021-10-29*
>
> **ABOUT MOD SUPPORT:** *We've got mod support, baby! Mod support is patron-only for now, and I'm not quite ready to give users the tools to make their own mods yet, but that's on the horizon. For now, I'm gonna release "official" mods to patrons to test out mod support and figure out some best practices and other issues. The first mod will be coming very soon, so if you're a patron, be on the lookout for it!*
>
> **ABOUT THE HAND-DRAWN ART PACK:** *A new art pack has been added! It is patron-only for now, but it will eventually come to the free edition. To use the new art pack, you may select it in the settings menu (hotkey:* `Z`*) or after starting a new game. You can switch between art styles freely at any time, though the change will only be reflected when the screen is updated.*
>
> This update features new story content, a special anniversary storyline, a couple of new requests, a new encounter type, a variety of new and expanded activities, and more! In addition, a new art pack has been added to the patron edition of the game, and mod support is entering beta testing for patrons as well!
>
> This update took a long time and faced multiple delays, and I'm sorry for that. I appreciate your patience and understanding, and I hope it ends up being worth the wait.

<p><details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/0100-anniversary.png" style="margin:auto;">
</details></p>
<p><details title="Click to reveal.">
	<summary>New Store Page Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/shelf/5.png" style="margin:auto;">
</details></p>
<p><details title="Click to reveal.">
	<summary>New Banners (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/banners/sixth.png" style="margin:auto;">
	<br><br>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/banners/sixth-tall.png" style="margin:auto;">
</details></p>

### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>To escape the loop, just keep clicking! It should be impossible to get permanently stuck in the loop. You can figure it out early if you make the right decisions at the right times, but you can also brute force it by repeatedly doing one action until it stops giving you new stuff, then repeatedly doing the other action.</li>
        <li>Talk to Roland in the bandit camp or tavern after breaking out of the loop to start the anniversary storyline.</li>
        <li>See the <u>Advancement</u> section of this changelog under the <em>Additions and Enhancements</em> heading for how to unlock the various new advanced trait lines.</li>
        <li>The new "flavor" of the gloryhole activity is random, with a low chance. Purchase the related traits to see it more regularly!</li>
        <li>You can do more than just sniff under garments if you have the right perks&hellip;</li>
        <li>During an erotic dance, if you keep dancing, eventually, the person you're dancing for will take matters into their own hands! Keep dancing for a while after the prompt to seduce them appears for an alternate ending.</li>
        <li>You have to be caught up on Wicked Pam's quests to play through the new request in this version! You also have to have visited her a few separate times. You can then find her in the market.</li>
        <li>Complete the above quest to unlock advanced transmutation and two powerful new potions!</li>
        <li><u>Orgasmic Suggestion</u> can be used to escape guards, make random NPCs cream themselves, or even to trigger an instant orgasm during sexual encounters (though it can usually only be used once per encounter do to the way lust works).</li>
        <li>You can unlock a new action for vaginal and anal sex encounters. If you want to unlock it, have lots of anal sex.</li>
        <li>There's a new random option that can occur during tongue baths in the barracks!</li>
        <li>If you need more tips or want to find out what you might be missing, ask on <a href="https://discord.gg/7BYqxYy" target="_blank">the discord server</a>.</li>
    </ul>
</details>

### Bug Fixes

- **[Sequence Break]** Paying rent should no longer break you out of prison or cause other strange "warps."
- **[Other]** Fixed some bugs, typos, and errors.
  - Fixed (again?) the issue with using special offers and not being able to complete the prologue storyline's potion selling task. I swear I fixed this before, but I ran into the issue myself on v0.9. It may have been a regression.
  - Fixed issue where shame emergencies were happening more often than intended with higher-tier perks.

### Content

- **[Story]** A small part of chapter 4 was added, mostly to set up the anniversary content.
  - <u>The Anniversary Storyline</u>: In addition to the above, there's also an optional storyline you can follow that will see you revisiting a place important to the past. Talk to Roland after starting chapter 4 to experience this content. This isn't quite a request, and it is optional, but it's also part of the story sort-of, so I'm not sure how to categorize it. Calling it a "storyline" for now.
- **[Requests]** I wanted to add more, but only one new request was added :(
  - Witness an argument involving a familiar face in the market in the evening, if you meet certain conditions.
- **[Encounter]** A brand new encounter type has been added: **lesbian sex**!
  - Dozens of new scene components for the new encounter type.
- **[Activities]** One new activity was added, and a few old activities have been expanded.
  - By taking a certain trait, there is now a chance the player character will steal the under garments of a sexual partner after an encounter. Under garments can also be found in other places in the world. These will be placed in your potion inventory, and can be used to trigger a brief scene of the player&hellip;"using" them. With the appropriate perks, the player character can use them to trigger a longer lewd event. Using these new items causes the player's lust to increase dramatically, and can also have other effects based on certain traits. This entire system is largely optional; if you don't like the idea, simply avoid the traits associated with it. You cannot craft these items, even though they are functionally similar to potions.
  - The gloryhole activity has been expanded. Now, when using the gloryhole, there's a chance for the player character to be&hellip;*particularly infatuated* with a client. This chance starts at 5% but can be increased all the way up to 50% via traits (the trait line requires access to the gloryhole activity and <u>Oral Expert 3</u> to unlock). The new activity is a slower, more detailed "dick worship" mini-game that's somewhat similar to the "tongue bath" activity already in the game. There's enough here that I'm including it as new content and listing it here instead of as an addition below.
  - A new erotic dancing activity has been added. This is related to one of the new advanced trait lines listed below. When nude, the player character can seduce some NPCs by dancing for them with this new activity.
- **[Art]** The new **hand-drawn art pack** is now available! It will only be in the patron edition for now, but should come to the free edition early next year. You will be prompted to select an art style when starting a new game, and you can also change art styles from the game settings (hotkey: `Z`) at any time.
  - Art can now be turned off entirely as well!

### Additions and Enhancements

- **[Alchemy]** New potions, new ingredients, new transmutations! Most of this stuff is locked behind a new request.
  - One basic new potion has been added. It's a small addition, and unlocked from the start of the game (or, from when you get the cauldron). It's a bit gross, so feel free to ignore it if it's not your thing. 
  - Two new "advanced" potions have been added. Their recipes are only learned after finishing a request and require new, more powerful materials to make&hellip; 
  - Two new ingredients that can only be acquired via transmutation have been added. Complete a request to unlock the ability to create these materials.
- **[Advancement]** Several new trait lines have been added, including all new "advanced" trait sets! This was a big update for traits.
  - **Sticky Fingers**, **Crime of Passion**, and **Gentlelady Thief** increase the chances that the player character will steal the under garments of a sexual partner after an encounter. Without the first trait in this line, she'll never steal them. Each trait increases the odds by about 25%.
  - **Under Appreciated** makes the player character immune to shame, lust, and hunger emergencies (but not urination emergencies) for the next three time periods after "using" undergarments.
  - **Decent Penis Case**, **Worthy Dick Holster**, and **Perfect Cock Pleaser** (I might have had too much fun with the trait names in this update) all increase the chances of a "particularly infatuating" person appearing at the gloryhole. The base chance is 5%, and each trait adds about 15% on to that, for a total of 50% with all three.
  - **Gatherer 1**, **Gatherer 2**, **Gatherer 3**, and **Master Gatherer** increase gathering efficiency, as you would probably assume.
  - **The Unclean Set:** A trait set focused around being, well, unclean. Requires **Master Semen Saint**, **Semen Boost 3**, and **Inspire Vigor 3** to unlock. After purchasing the first trait, you can purchase the remaining three in any order.
    - **Unclean: Sloppy Seconds** boosts the sexual pleasure the player character gives partners during encounters when she's dirty. The dirtier she is, the greater the effect.
    - **Unclean: Enticing Filth** grants a large increase to seduction when dirty. The dirtier she is, the larger the bonus.
    - **Unclean: Satisfied Slut** being dirty slows the rate at which the player character gets hungry. The dirtier she is, the greater the effect.
    - **Unclean: Field Alchemy** allows the player character to perform basic transmutation and alchemy using the filth on her body. She instantly cleans herself and gains a basic potion from a small list. Requires **Basic Transmutation**, which is a request reward.
  - **The Nymph Set**: A set focused around lust and horniness. Requires **Master Sex Maniac**, **Sex Master**, and **Master Whore** to unlock. After purchasing the first trait, you can purchase the remaining three in any order.
    - **Nymph: Contagious Lust** improves a partner's lust during an encounter based on the player character's current lust level.
    - **Nymph: Always Horny** removes the penalties to social skills incurred by being horny or hungry.
    - **Nymph: Chain of Lust** increases the chance of chains occurring during prostitution and the maximum number of customers that can be chained. The bonuses are relative to the player character's current lust.
    - **Nymph: Orgasmic Suggestion** the player can induce orgasms in NPCs and sexual partners. Doing so requires the player character to have a high level of lust, and using this ability reduces her lust immediately. This ability can be used during encounters, but also on some NPCs. Additional opportunities to use it will be added in future updates.
  - **The Nudist Set**: A set focused around being naked and exhibitionism. Requires the **True Whore** perk and one of the other advanced traits to unlock. After purchasing the first trait, you can purchase the remaining three in any order.
    - **Nudist: Sex Appeal** gives partners in sexual encounters a large boost to lust at the start of an encounter if the player character is nude.
    - **Nudist: Erotic Entertainer** gives the player character to dance seductively for NPCs as a means of seducing them if she is naked. This can be used on several NPCs, including guards! More opportunities to use it will be added in future versions.
    - **Nudist: Making a Scene** grants the player character a boost to her own pleasure when having sex in front of a crowd, and also increases performance ratings for sexual encounters that occur in public.
    - **Nudist: Naturalist** boosts the player character's gathering efficiency in the lake and forest when they are naked.
- **[Activities]** Some activities have been expanded upon.
  - Prostitution has been expanded: women can now appear as customers. Female customers trigger the new lesbian encounter, as you'd probably imagine. Futanari content will be added to prostitution in a future update, but for now is not possible to do in the prostitution activity (but it still works in most other activities). You can adjust the odds of female customers appearing in prostitution via the game settings (hotkey: `Z`). It's possible to make female customers never appear by setting the slider all the way to the left, and it's also possible to make all customers women by setting it all the way to the right.
  - Some old activities have been improved, received additional content, or had new events added. These include maid work, tongue baths in the barracks, some locations, and some status reactions.
- **[Encounters]** New actions and scenes!
  - A new action has been added to vaginal and anal encounters, along with several new scenes.
  - New orgasm and crowd scenes have been added to various encounter playlists.
  - Many other playlists have received new, adjusted, corrected, or improved scenes.

### Balance and Progression Adjustments

- **[Alchemy]** Gathering rates for most materials have been adjusted for both the lake and the forest.
- **[Economy]** Some potion prices were adjusted.
- **[Encounters]** In an effort to make the encounter system more varied and interesting, and in laying the ground work for partners to actually have preferences in the future, the following adjustments and changes have been made:
  - Several internal changes have been made to how actions are evaluated in terms of orgasm build-up and critical rates. Actions now have flags associated with them that determine what their critical rates will be for both the player and partners based on the kind of action it is. This has made criticals a more common occurrence.
  - Because criticals are now slightly more common, most actions have seen reductions in terms of orgasm build-up across the board. These reduction actually may not be enough as criticals seem a bit common right now. There will be ongoing balance changes over the next few patches.

### Performance and Technical Changes

- **[Executable]** Mod support system has been integrated. Player mods will be coming soon (as in before the end of the year), but I need to thoroughly test what we have now before I go much farther, so for now I will be releasing mods I make myself to patrons to test them. Mods will only be available in the executable version of the game, not in the web version. Mod support will *eventually* come to the free edition, but there is no ETA on that. It's gonna be months; probably no earlier than sometime next year. Here's what's currently supported by mods:
  - New events added to existing playlists.
  - New encounter scenes added to existing encounter types.
  - New encounter scene playlists (player only).
  - New event playlists.
  - New actions in most in-game areas.
  - New player actions in all encounter types.
  - New perks and traits (rough).
  - Arbitrary passages can be added and linked to, so it's possible to add other things, like new areas, activities, or story content.
- **[General]** Many, many internal engine improvements. (Mostly thanks to mod support!)

## v0.9.1 (patch)

> **v0.9.1** &ndash; *2021-07-01*
>
> This patch brings the **Lady of the Rain** update to the free edition of the game! No other new content has been added.

### Bug Fixes

- **[Other]** Corrected some typos and other issues.

### Performance and Technical Changes

- **[Executable]** Updated NW.js shell to version 0.54.

## v0.9.0 (update)

> **v0.9.0** &ndash; **Lady of the Rain** &ndash; *2021-06-24*
>
> The truth has finally been revealed, and now that the identity of Lady Pelenor has come to light, the player character and her comrades won't be safe until they find a way to stop her. Who will the player character turn to as a final confrontation looms on the horizon? And for what purpose has Lady Pelenor been building her power and biding her time?
>
> In addition to brand new story content, this update also adds new activities, events, requests, and scenes to the game. Tons of brand new artwork has also been added, and all older artwork has been remastered and improved dramatically.  

<details title="Click to reveal.">
	<summary>Key Art</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/090-lady.png" style="margin:auto;">
</details>
<details title="Click to reveal.">
	<summary>New Store Page Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/shelf/4.png" style="margin:auto;">
</details>


### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>As you play through the plot of this update, you will be prompted to visit one or more of your allies to ask for advice. Refer to the log (hotkey: <code>E</code>) to see which allies you can talk to. You must talk to all of the available allies to move on. After talking to all available allies, you'll be prompted to speak to Roland at the tavern (or at the bandit camp, if the tavern is destroyed).</li>
        <li>The allies you can visit, and therefore the plans you can form, are based on your relationships, choices, completed requests, and other factors. It is possible to complete certain tasks or build up certain relationships to unlock new paths before speaking to Roland. If you speak to Roland, you will be prompted to select an approach, and will no longer be able to unlock new paths.</li>
        <li>Some of the paths you unlock will feature unique scenes, discussions, and even flashbacks that provide additional context and scenes that are possible to miss and may influence events even if that path is not chosen!</li>
        <li>The path you select will have a profound effect on the events of chapter 4 and 5, so it's an important choice. It is recommended you create a save file before choosing an approach for future use, though a New Game + mode will be added to the game in a future update as another way to try different endings.</li>
        <li>Visit Wicked Pam in the witches' coven to start a request and eventually unlock transmutation and the ability to create a homunculus.</li>
        <li>You can unlock Wicked Pam's shop by completing a request that can be started by talking to Charlotte in the bandit camp.</li>
        <li>Head to the lake in the evening and talk to Priscilla to start a request and unlock the broadcasting ability.</li>
        <li>Broadcasting allows you to lower your lust level by "broadcasting" it in public spaces, but doing so causes others to feel that lust in your place. What specifically happens depends on how many people are around, the time of day, and how much lust you have.</li>
        <li>You can unlock the ability to transmute semen into ale or gem dust. Additional recipes for transmutation will be added in future updates, most likely as request rewards. To transmute, unlock it via the request mentioned above, then begin alchemy in your home and a new menu screen will be shown allowing you to select transmutation.</li>
        <li>After unlocking the ability to create a homunculus, you can do so in your home. Only one can exist at a time, and their tasks typically last for one whole day (3 time periods). You can have the homunculus serve a dungeon sentence on your behalf, or have it collect ingredients for you. You can also lewd it, if you want :).</li>
        <li>You can unlock a perk, gained by having sex as a futanari, that allows you to use futanari potions on NPC women via potion magic. You <strong>need to unlock potion magic to do this</strong>!</li>
        <li>If you need more tips or want to find out what you might be missing, ask on <a href="https://discord.gg/7BYqxYy" target="_blank">the discord server</a>.</li>
    </ul>
</details>

### Bug Fixes

- **[Other]** Fixed some bugs, typos, and errors.

### Content

- **[Story]** This update features the rest of chapter 3. You will have to make a **very important decision** in this update's story content, so choose wisely, as this choice will effect the entire rest of the game's story for you. See the hints section above for more information.
- **[Requests]** Two new requests have been added.
  - Visit Wicked Pam (the option to visit her is itself unlocked in a request) in the witches' coven to learn some new alchemy techniques.
  - Go to the lake in the evening after Priscilla moves to the bandit camp to find her meditating and join her.
- **[Activities]** Some new activities have been added.
  - After a certain request, you unlock the ability to <u>create a homunculus</u> which can perform certain actions for you. You can also have sex with it in a few different ways.
  - After a certain request, you unlock the ability to <u>broadcast</u>, which adds new voyeur-style content to the game and also has a functional aspect as it allows you to reduce the player character's lust.
  - You can now unlock the ability to use <u>futa potions</u> on NPC women via a perk.

### Additions and Enhancements

- **[Alchemy]** You can now unlock the ability to transmute semen into certain other ingredients. The selection of options is very limited right now, but future updates should expand this, probably through additional requests.
- **[Advancement]** New perks and traits have been added.
- **[Media]** Many art additions and improvements are included in this update&hellip;
  - A ton of new CGs have been added for homunculus sex, homunculus futanari sex, and NPC futanari sex.
  - The status images, viewable by clicking the character portrait or by opening the character menu (hotkey: `Q`), have been replaced with totally new images featuring improved posing, lighting, and image quality.
  - **All** of the game's existing portraits and CGs have been "remastered" and improved. The art hasn't been completely redone, but it has all been upscaled, touched up, and de-noised, mostly with automated tools, though some hand-edits were also made.

### Balance and Progression Adjustments

- **[Alchemy]** Alchemy progression has been entirely reworked. From [this post](https://www.patreon.com/posts/50792434): *"&hellip;[The] current points and tiers will remain, but different potions will be worth different amounts of "points" and certain potions would stop being worth points altogether as you increase your tier.&hellip; [You] will have to make more advanced potions to progress, rather than just a lot of potions. The number of potions needed would be less overall, but be harder to make, so ultimately this [will] slow down the alchemy progression."* The overall net change is that progression is faster but makes a bit more sense.
- **[Social]** The shame system has been reworked. As described in the [update preview](https://www.patreon.com/posts/52528317): *"First, the cooldown for shame emergencies is increased, so the minimum time between such emergencies is much longer. Second, the actual base chance of shame emergencies occurring has been reduced by about 33%. Finally, having certain exhibitionism-related perks further decreases the base chance of an emergency. Each of the last three perks (nudist, no shame, and true whore) reduce the the chance of a shame emergency occurring by about 25% each. This should reduce the rate of shame emergencies to about 2.5% when you have the 'true whore' perk. This is a massive reduction in paper, but when playing the game, I still did encounter shame emergencies with some regularity when I was trying to trigger them, so if you like them, you should still be able to get some. I may introduce a trait set or a potion to counteract these changes in a future update."*

## v0.8.1 (patch)

> **v0.8.1** &ndash; *2021-05-01*
>
> This patch brings the **Growing a Pair** update to the free edition. Also included are some balance and progression changes, along with a few bug fixes. No new content has been added compared to the patron-edition release.

### Bug Fixes

- **[Other]** Minor bug fixes and corrections.
  - Fixed some minor progression issues for certain requests and plot tasks.

### Balance and Progression Adjustments

- **[Alchemy]** The highest tier rewards in gathering are now more likely to be pulled (see below for the numbers). This has the net effect of making gem dust and large amounts of herbs/spring water easier to get.
  - Highest tier rewards used to have an 8% pull chance, now 12%.
  - Second-highest tier rewards used to have a 15% pull chance, now 20%.
  - The other tiers are unchanged.
- **[Activities]** Expedition party members gain XP at a markedly higher rate (an increase of approximately 20%).
- **[Social]** Social events have been reordered in terms of priority to help provide a broader variety of event types with greater consistency.
  - Shame events should be slightly less likely.
  - Status events are slightly more likely.
  - Notoriety events are more likely, especially during chapter 1 when they are needed for story progression. 

## v0.8.0 (update)

> **v0.8.0** &ndash; **Growing a Pair** &ndash; *2021-04-22*
>
> This update covers the first part of chapter 3&mdash;meaning we've reached the halfway point of the game's main story! Having finally gained access to the castle, the protagonist can finally confront Lord Pelenor. But what waits for her in the castle is going to be a lot more than she bargained for&hellip;
>
> In addition to the new story content, several other new additions have been made. One of the largest is the addition of futanari content; using a potion, the protagonist can temporarily gain male genitalia before certain scenes or events. Also added are new requests, new activities, new character progression, several new potions, new images and music, and more.
>
> **Note:** Following this update, the game will be changing from a monthly update schedule to a longer, 6-8 month update schedule. This means the next major update to the game won't be until sometime in June! With this change, hopefully I'll be able to bring better, more polished content while also having additional time to work on secondary concerns like mod tools.

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/080-growing.png" style="margin:auto;">
</details>


### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>If you want to romance Priscilla, you have to do so before moving on to chapter 3.</li>
        <li>To unlock futanari content, complete the brief request "Growing a Pair" in the market.</li>
        <li>Futanari options will only be displayed if you've completed the aforementioned request and you're carrying a "futa potion."</li>
        <li>Futanari options have been added to generic female interactions (administer certain potions to female NPCs, first), most forms of masturbation, and flashing. There are also a small number of events that feature these options, with more coming in the future.</li>
        <li>The "Own Brand" potion and the "(Own Brand)" variants of some potions have unique effects and can be improved with a trait.</li>
        <li>You can unlock a "tongue bath" activity where the player character licks guards clean in the barracks my completing a request found in uptown at night.</li>
        <li>Several new potions have been added! It's been a while since we've had major potion additions. Make some potions to unlock them. There should be two new tiers!</li>
        <li>The new "sloppy lubricant" causes the player character to "leak" a semen-like fluid as she walks around and dramatically improves sexual prowess.</li>
        <li>"Deviant ale" allows the player to avoid all shame emergencies and behaviors in exchange for making her lust high for the effect's duration. It has a diminished effect on the flashing activity.</li>
        <li>Finally, "jizz bombs" can be thrown at some NPCs during certain events. They may even help you escape guards.</li>
        <li>If you need more tips or want to find out what you might be missing, ask on <a href="https://discord.gg/7BYqxYy" target="_blank">the discord server</a>.</li>
    </ul>
</details>

### Bug Fixes

- **[Other]** Fixed some bugs, typos, and errors.
  - Character portraits were bugged for some players. It should now work again.
  - Fixed some minor issues where certain events would return players to the incorrect locations.

### Content

- **[Story]** The first part of chapter 3 is here. The protagonist has finally gained access to the castle, and can finally confront Lord Pelenor. But what waits for her in the heart of the city is far more dangerous than any noble, and will confront her with a long forgotten truth.
- **[Requests]** Two new requests have been added.
  - See what the men in the market are lining up to buy. *Unlocks futanari content!*
  - Talk to a maid in uptown at night.
- **[Activities]** The new futanari content is the major addition in this update. While not a specific activity per se, futanari options have been added to several events and activities. I intend to expand these options in future updates as well. To see futanari options, the player must complete a specific request and have a **futa potion** on hand.
  - Futanari options have been added to: masturbation activities (public, outdoor, and at home), sex with female characters (player option only for now), flashing, and some miscellaneous events.
  - There's a new activity in the barracks unlocked by a request.
- **[Events]** Some new events and scene variants have been added to various event playlists and activities.

### Additions and Enhancements

- **[Alchemy]** Several new potions have been added. Some highlights:
  - **Deviant Ale** &ndash; Maxes out the player character's lust for a period of time, but during that time she is immune to shame and shame emergencies.
  - **Jizz Bomb** &ndash; Can be thrown at certain NPCs. Will generally gross them out and irritate them. May help you get into and out of trouble in certain situations.
  - **Sloppy Lubricant** &ndash; Makes the player character extra lubricated, increasing the sexual pleasure she can deal in encounters, but also makes a bit of a mess.
  - **Futa Potion** &ndash; Causes the player character to grow male genitalia temporarily. Can only be used prior to or during certain scenes and activities. In a future update, you will be able to target female NPCs with it.
  - **Semen Pearl**, **Own Brand**, **Bottled Stench** &ndash; There are a few other, less substantial potion additions too. Try them out!
- **[Advancement]** New advancement opportunities, mostly related to futanari content.
  - Several new traits have been added, mostly related to the new futanari content. The player can get a trait allowing them to have larger-volume ejaculations as a futanari, and a trait that effects some of the properties of their "Own Brand" of ejaculate. The new requests also have new traits associated with them, as per normal.
  - New sexual experience and fetish development for futanari content.
- **[Encounters]** Sexual encounters that take place in public now have a "crowd" actor that will dynamically participate in the scene. Future additions in future updates will hopefully make the crowd smarter and more convincing, increasing immersion. For now, I think it's very much an improvement, but the crowd has a lot more general behaviors and comments than reactive ones.
- **[Interface]** The dark theme is now the game's default theme.

## v0.7.3 (patch)

> **v0.7.3** &ndash; *2021-03-26*
>
> Minor clean-up of the last patch and fixes for dark mode interface issues.

### Bug Fixes

- **[Interface]** Fixes and tweaks.
  - Fixed dark mode issues with image rendering.
  - Clean up and minor fixes.
  - Minor tweaks to dark mode for readability.

## v0.7.2 (patch)

> **v0.7.2** &ndash; *2021-03-25*
>
> This patch addresses a major interface issue.

### Bug Fixes

- **[Interface]** Fixed button placement issue on the encounter interface.

## v0.7.1 (patch)

> **v0.7.1** &ndash; *2021-03-25*
>
> This patch fixes a few bugs, adds some new character portraits and CGs, and adds a bit of new content, too. And the **Exposed** update has also come to the free edition of the game as of this patch.

### Guide: Priscilla Romance

Priscilla can finally be romanced as of this patch. If you make the appropriate decisions, she will fall in love with the player character! To get to this point, you need to complete certain requests and maintain a good relationship with her. The following is a detailed guide, since I think people will probably ask about it.

<details title="Click to reveal.">
    <summary>Guide (Spoilers!)</summary>
    <p>
        Note: If you need to see if you've completed a request, check your list of purchased traits for the associated special trait. 
    </p>
    <ol>
        <li>Complete the request "Preservation." This is required to move on in the story, and requires you to meet up with Priscilla at various locations in the game world. Grants the trait "preservation."</li>
        <li>Complete the request "Preservation II." To complete this request, you have to create a Lactical potion and use it on the player character to induce lactation. You must also create and have on hand a Succubus Snack potion. Wait until the player character can lactate (it builds over time) and then visit Priscilla. Grants the trait "milk maiden."</li>
        <li>Complete the request "The Sting." You can start this quest in the barracks by talking to a guard and giving him a Sleeping Potion. Grants the trait "leeway."</li>
        <li>Complete the request "Witches' Coven." You can start this quest by finding Priscilla in the forest after completing "The Sting" and "Preservation." Grants the trait "wicked witch."</li>
        <li>Move through the story until you have to meet up with Rais to tell him that Lord Wimbol has the royal heirloom toward the end of chapter 2. After meeting with Rais, he will tell you to meet him in uptown. From this point in the story, the request "BFFs" will become available if Priscilla likes you enough.</li>
        <li>Once you unlock Priscilla's enchantment ability, you're a little more than halfway to her liking the player character enough to see the "BFFs" request. You can give potions to Priscilla to improve her affinity for the player character. If you've completed everything else up to this point, try raising her affinity. If you generally are nice to Priscilla, or act romantically toward her, her affinity may be high enough as soon as you get to this point!</li>
        <li>Complete the request BFFs. Toward the end, Priscilla will confess to the player character. At this point, choose to reciprocate her feelings and the player character will start a romantic relationship with her.</li>
    </ol>
    <p>This may seem complex, but it's really just a quest chain. The only weird thing I think people may miss is "Preservation II," because the requirements to start it are fairly strict, and the part regarding "The Sting" since it may not be readily apparent that Priscilla is involved in that request.</p>
</details>

### Bug Fixes

- **[Interface]** Fixed a longstanding bug with  image rendering so that images should no longer be cut-off in certain events.
- **[Other]** Some other small improvements and bug fixes.

### Content

- **[Requests]** A new request has been added! Talk to Priscilla after meeting certain conditions to find it. <span title="Major Spoiler!" style="filter:blur(4px);cursor:pointer;" onclick='this.style.filter = ""'>The request is only unlocked if you've completed every other Priscilla request and she has a very high affinity for you. You can increase your affinity with her by giving her potions for free.</span> 
- **[Art]** Three new CGs have been added for a special new activity, unlockable in the new request!
- **[Other]** Portraits!
  - Portraits for Gray have been added and mostly implemented.
  - Portraits for Charlotte have been added and mostly implemented.
  - Portraits for Priscilla have been changed.

### Performance and Technical Changes

- **[General]** Internal improvements.

## v0.7.0 (update)

> **v0.7.0** &ndash; **Exposed** &ndash; *2021-03-18*
>
> The March update adds a ton of new exhibitionism content to the game! You can now engage in a flashing activity when in public spaces, and a new shame mechanic and status will help improve the game's feedback when the player character is engaged in most kinds of public indecency. Many existing features related to exhibitionism, nudity, and streaking have also been overhauled or improved to better fit with these additions, along with various other small tweaks and changes.
>
> This update also closes out chapter 2, featuring all-new story and side content. Also added are new portraits with new expressions for the player character, new events, new activities, new advancement options, and new requests. Some rather dramatic engine improvements also accompany this update, hopefully reducing any lag issues users have experienced, especially on lower-powered machines.
>
> **IMPORTANT:** The game now autosaves less frequently by default. You can configure your autosave frequency in the game settings. Please make sure to save the game before closing it!

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/070-exposed.png" style="margin:auto;">
</details>


### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>If Lord Wimbol owes you a favor from your dealings with him back in chapter 2, you'll gain access to a special scene.</li>
        <li>If the bandits like you enough, you can gain access to their group sex activity.</li>
        <li>You can now engage in the flashing activity in certain locations with the correct perks.</li>
        <li>There are a variety of scenes associated with shame emergencies.</li>
        <li>Generally, being nude and dirty is the best way to build up shame.</li>
        <li>Head to the backstreets when you're filthy to start a request their that will unlock a new activity.</li>
        <li>The new backstreets activity has several different possible actions based on player perks! Try coming back to it from time to time.</li>
        <li>The level cap and party cap of expeditions has been increased.</li>
        <li>Talk to Charlotte and offer to help her get a sleeping potion to unlock new faction activities in the bandit camp.</li>
        <li>Try clicking on the character portrait!</li>
        <li>If you need more tips or want to find out what you might be missing, ask on <a href="https://discord.gg/7BYqxYy" target="_blank">the discord server</a>.</li>
    </ul>
</details>

### Bug Fixes

- **[Other]** Fixed some typos and a few small issues.
  - Some completed requests were not properly cleared from the game log, this should now be fixed.

### Content

- **[Story]** The second and final part of Chapter 2 is here! The protagonist joins Captain Rais to confront Lord Wimbol about the stolen royal heirloom. But the confrontation presents her with a strange mystery from the past and raises more questions then it answers.
- **[Requests]** Two new requests have been added.
  - Get very dirty and head to the backstreets.
  - Charlotte could use some help in the bandit camp.
- **[Activities]** A few new activities have been added.
  - Flashing has been added as a repeatable activity in a few locations. The player must manage the player character's shame and lust while performing various embarrassing and lewd actions. The more public the place and time, the more difficult the activity is.
  - A new activity has been added to the backstreets.
  - The bandit camp now features a group sex variant.
  - After completing a request, you will be able to trade sleeping potions to Charlotte for G and faction reputation in the bandit camp.
- **[Other]** New events and side content.
  - Two new sets of portraits have been added to the protagonist. *Known issues:* the portraits may not always show the correct level of hygiene. This will be fixed soon.
  - A new playlist of events for the new activities and for the new shame status emergencies.

### Additions and Enhancements

- **[Interface]** The portrait system has been improved in several ways and the game's general appearance has been updated.
  - Portraits now feature drop shadows to help them stand out.
  - Portraits now glow pink when the protagonist is embarrassed to help indicate the level of shame. *Known issue:* Sometimes the portrait's pink glow gets stuck and remains even when it shouldn't. Restarting the game fixes the problem.
  - Portraits can also glow other colors, when appropriate.
  - The player character's neutral expression is now replaced with blushing expressions based on her level of shame. Non-neutral expressions, however, take precedence over the blushing effect.
  - You may now click on the portrait to see an expanded full-body status image (the same one featured in the character menu). Click again to revert. *Known issues:* (1) The status image is always of the protagonist, even when the subject of the portrait is someone else. (2) The status image behaves oddly on mobile devices.
- **[Expeditions]** The maximum level for expedition party members has been raised to 7 (up from 5), and the maximum party size can now reach 7 (up from 6) based on your overall expedition rank. Higher level expedition parties have improved gather rates.
- **[Social]** A new status has been added: **shame**. The player gains shame by being nude or dirty in public places. The amount of shame is increased in places with higher populations. Public indecency and exhibitionism also increase shame. Shame is reset to zero if the player character is dressed and clean, or if she is in a non-public place.
  - Shame has two levels: *embarrassed* and *ashamed*. When embarrassed, the player character gains lust over time at an elevated rate and takes a minor penalty to social skills. When ashamed, both of these effects are dramatically increased, and the player character becomes susceptible to shame emergencies.
  - Like other consequences or emergency behaviors, the player briefly loses some control of the player character and some time is wasted. In the case of shame, one of a few different events can occur, usually involving the player hiding or running away.
  - As mentioned above, shame does not have a gauge in the status bar, but the character's expression and blushing in the portrait, plus the pink glow around it, will communicate the current level of shame.
- **[Advancement]** New perks have been added and old perks have been shuffled around, all in regards to the exhibitionism fetish. The new and realigned perks are as follows:
  - **Flasher:** With this perk you can start the flashing activity in public places at night, or certain other, similarly desolate locations.
  - **Bold Flasher:** This perk allows the flashing activity to be started even in public spaces with a lot of people and during the day.
  - **Nudist:** You can leave your house naked and go streaking in the market.
  - **No Shame:** Allows public masturbation and urination, and some other public lewd acts.
  - **True Whore:** Allows public sex, among other more overt public acts.

### Performance and Technical Changes

- **[General]** Internal engine improvements. The game should now run smoother and faster across the board, with fewer instances of noticeable lag.
- **[Other]** **Autosaves are now less frequent by default** (they occur in certain scenes/places, at every 3 minutes). Users may configure this frequency in the settings menu. Lower frequencies should improve game performance.
- **[Executable]** Updated NW.js shell to version 0.52.

## v0.6.1 (patch)

> **v0.6.1** &ndash; *2020-02-16*
>
> This patch brings the **Bandita** update to the free edition of the game! No other new content has been added.

### Bug Fixes

- **[Other]** Corrected some typos and other issues.

### Content

- **[Other]** Minor editorial changes.

## v0.6.0 (update)

> **v0.6.0** &ndash; **Bandita** &ndash; *2020-02-09*
>
> To get her revenge, the protagonist needs to get into the castle, and to get into the castle, one of the royals or nobles inside has to invite her. Teaming up with her allies, the protagonist hatches a daring plan to gain an audience with the king, but before it can be set in motion, things go horribly wrong. Now the target of powerful enemies, the player character has to choose whether to stick to the plan or lay low until things blow over&hellip;
>
> In addition to brand new story content featuring several meaningful decisions, this update also includes a brand new area, new characters, a new request, several new events and activities, new progression opportunities, and more!

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/060-bandita.png" style="margin:auto;">
</details>

<details title="Click to reveal.">
	<summary>New Store Page Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/shelf/3.png" style="margin:auto;">
</details>


### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

**Warning:** Certain requests may become impossible to complete after getting to a certain point in chapter 2! 

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>There is a major branch in this part of the story that has far-reaching effects. Some of these effects are enumerated below, but others are more subtle or may come in future installments.</li>
        <li><span title="Major Spoiler!" style="filter:blur(4px);cursor:pointer;" onclick='this.style.filter = ""'>If you choose to leave town with Roland</span>, you'll find a new place to live, rent-free. Additionally, you'll be able to re-open the tavern for free, but without any option for co-ownership.</li>
        <li>On the other hand, <span title="Major Spoiler!" style="filter:blur(4px);cursor:pointer;" onclick='this.style.filter = ""'>if you decide to stay</span>, you'll keep your living arrangements the same. The tavern will not be automatically re-opened, and you'll have to help Roland fund its re-opening, but you'll gain a stake in the tavern's ownership and a weekly stipend from Roland.</li>
        <li>Depending on certain choices you make and on certain other circumstances, as many as several areas will be temporarily inaccessible over the course of the story content in this update. Simply continuing the story will make mot of these areas available again. No areas are permanently removed, and all areas can ultimately be restored (though some may be "moved").</li>
        <li>After reaching a certain point in chapter 2, some requests and side content related to Carol and Gray may not be completable until much later in the game (sometime in chapter 3, which is not available yet). Please finish up any requests involving these characters before meeting Gray at the tavern.</li>
        <li>It is possible to correctly guess the identity of <span title="Major Spoiler!" style="filter:blur(4px);cursor:pointer;" onclick='this.style.filter = ""'>the noble who has the heirloom before Charlotte tells you</span> if you perform specific actions leading up to this scene.</li>
        <li>You can unlock a source of a certain ingredient (and some lewd activities) in the bandit camp with the appropriate perks.</li>
        <li>A new activity has been added to the guard barracks, but it requires a certain perk to unlock.</li>
        <li>If you consider leaving town for an extended period, don't forget to say goodbye to a certain someone. Likewise, make sure to stop in on your buds when you get back!</li>
        <li>Talk to Charlotte after unlocking the bandit camp for a request. Completing it grants you access to a new special vendor!</li>
        <li>If you need more tips or want to find out what you might be missing, ask on <a href="https://discord.gg/7BYqxYy" target="_blank">the discord server</a>.</li>
    </ul>
</details>

### Bug Fixes

- **[Advancement]** Fixed a few traits that weren't working as intended.
- **[Activities]** Carol's special offer was not giving the correct bonuses, it should now be fixed.
- **[Other]** Fixed some typos and a few smaller bugs. Also:
  - Certain dead ends due to bugs, particularly with the consequences system, have been fixed.
  - Fixed a few issues where the wrong materials or ingredients were being awarded (e.g., semen from females, wrong kind of urine, etc).
  - Some issues with images not displaying or loading properly in both the web and executable version of the game should now be fixed.
  - The guard barracks should now correctly show the protagonist's status portrait, like other areas.

### Content

- **[Story]** The first part of Chapter 2 is here! The player character teams up with Gray to steal a valuable item back from a group of notorious thieves, but things go wrong before the plan is even hatched. This leg of the story features a lot of branching with some pretty far reaching consequences. You may even want to save-scum a bit to check out the different paths and see what the more immediate results are!
  - Some editorial changes and improvements have been made to certain parts of chapter 1.
  - Certain social skill checks in the prologue and chapter 1 are now easier.
- **[Requests]** One new request has been added. After gaining access to the bandit camp, talk to Charlotte.
- **[Other]** A new area, new activities, and new events.
  - Some new events were added to the status and reputation reaction playlists. 
  - Most of the new activities have their own new playlists to pull events from featuring all-new event content. 
  - Some editorial and content improvements were made to existing events across a few different playlists.
  - A new area has been added: the bandit camp.

### Additions and Enhancements

- **[Advancement]** A mysterious vendor can be found in the coven after certain conditions are met. She sells items, materials, and potions in exchange for trait points (TP).
  - A new special trait has been added as a request reward.
  - A new potion is available: the **Suggestion Potion** which is powerful and briefly forces NPCs to do exactly what they're told. It cannot me made by the player yet, but it can be purchased via the TP exchange. The player will eventually be able to make them.
- **[Activities]** A few new activities have been added.
  - A new activity has been added to the barracks. Fair warning: it may not be everyone's cup of tea, it's a bit&hellip;*specific*, even by the standards of this game.
  - A new activity has been added to the coven in the forest.
  - The new bandit camp area has been added. An activity revolving around watersports has been added there, along with some smaller things to do. The camp will be expanded in future updates, and eventually feature several requests, faction activities, and a group sex variant, similar to the other faction bases (that is, the guard barracks and the witches' coven).
  - Some changes will occur at the tavern over the course of chapter 2's story progression. The tavern may also be inaccessible temporarily, but can be re-accessed with almost all the previous features and activities.
- **[Media]** New music tracks have been added.
- **[Other]** You now have the option to exit directly to the hub from the coven, without needing to go through the forest.

### Balance and Progression Adjustments

- **[Advancement]** Rebalanced the "Sex Maniac" line of traits. This is an overall nerf, but the increases per rank are now more even across the trait line.
- **[Other]** The "Milk Maiden" special ability has been improved and should now be more reliable, triggering the love potion effect more often and reducing the chances of nothing happening.

### Performance and Technical Changes

- **[General]** Some performance and stability improvements.
- **[Executable]** Updated NW.js shell to version 0.51.

## v0.5.1 (patch)

> **v0.5.1** &ndash; *2020-12-18*
>
> This patch brings the **Witches' Coven** update to the free edition of the game and adds a few new events.

### Bug Fixes

- **[Other]** Fixed some typos.

### Content

- **[Other]** Some minor content additions and changes.
  - Added some new scenes to the new encounter type.
  - Some minor editorial changes and fixes.

## v0.5.0 (update)

> **v0.5.0** &ndash; **Witches' Coven** &ndash; *2020-12-11*
>
> This update introduces a bunch of new side content! While no new story content was added in this update, the volume of side content added should make up for it! Included are four new requests, four new activities, several new events, and even a brand new encounter type! Dedicated activities for group sex content (in both lesbian and heterosexual varieties, with a bisexual/mixed activity coming soon) are among the newly added activities, along with a new "potion bounty" system!
>
> Also included in this update are two frequently requested quality-of-life improvements: request tracking in the game log and built-in tips on suggested activities to unlock new perks. There are also new event variations, new artwork (including portraits for Priscilla), new perks and traits, and more!

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/050-witches-coven.png" style="margin:auto;">
</details>

<details title="Click to reveal.">
	<summary>New Store Page Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/shelf/2.png" style="margin:auto;">
</details>


### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>To unlock the coven area, and potion bounties, complete a certain request started at the barracks.</li>
        <li>To unlock group sex content at the barracks, you need to increase your reputation with the guards.</li>
        <li>To unlock group sex content at the coven, you need to increase your reputation with the witches.</li>
        <li>To increase your reputation with the guards, you can give or sell potions to Dale, or complete certain requests.</li>
        <li>To increase your reputation with the witches, you can complete potion bounties at the coven, or complete certain requests.</li>
        <li>Increasing your reputation with these factions also has other effects!</li>
        <li>Carol's odds of graduating the maid academy are based on your maid skill. If she fails to graduate, you will no longer be able to work in the tavern as a barmaid.</li>
        <li>To "hire" Carol, go to the market, set up shop, and turn on her <u>special offer</u> at the bottom of the selling interface.</li>
        <li>Carol will only be permanently available as a <u>special offer</u> after completing her request.</li>
        <li>The new encounter type must be unlocked by earning a special perk. Many returning players will likely immediately unlock it, as its requirements are fairly low. New players can unlock it fairly quickly. The new encounter type has a few unlockable actions, like other encounter types.</li>
        <li>To gain the ability to freely enter and leave the dungeon, talk to the matron to start a certain request. To start it, you must meet certain conditions.</li>
        <li>If you need more tips or want to find out what you might be missing, ask on <a href="https://discord.gg/7BYqxYy" target="_blank">the discord server</a>.</li>
    </ul>
</details>

### Bug Fixes

- **[Social]** Fixed some bugs in the social skill calculations.
- **[Other]** Fixed some typos and a few small issues.

### Content

- **[Requests]** Four new requests.
  - Head over to the barracks at a certain time and bump into Gray.
  - Head over to the barracks at a certain time to meet a "cursed" guard.
  - At the coven, talk to the matron.
  - Visit Carol at Gray's home in the backstreets to help her settle in.
- **[Encounter]** A brand new encounter type.
  - Dozens of new scene components for the new encounter type.
  - 5 new CGs for the new encounter type.
- **[Other]** A few new events across certain playlists.
  - Portraits for Priscilla have been added. In all, six variations.

### Additions and Enhancements

- **[Advancement]** New advancement opportunities have been added.
  - New encounter type experience growth has been added.
  - A new line of traits for skill with the new encounter type.
  - Several new special traits that can be earned by completing requests.
  - Three new perks to unlock, all related to the new encounter type.
- **[Activities]** A few new activities have been added.
  - The player can now unlock group sex and gangbang related activities in the guard barracks by improving her relationship with the guards (by completing certain requests).
  - The player can unlock the witches' coven sub-area in the forest by completing a related request (this request is started at the barracks).
  - The player can unlock a group sex activity in the coven as well by improving her relationship with the witches.
  - The player character can take on "potion bounties" at the coven for large amounts of G and to improve her relationship with the witches. The player is limited in how often they may accept a bounty, and they may only accept one at a time. Bounties can be reviewed in the game log (hotkey: `E`). Return to the coven's bounty board once you have the requested potions to cancel or complete it. Canceling bounties has a small negative effect on your standing with the witches, while completing them has a large positive effect.
  - A brand new <u>special offer</u> has been added to setting up shop in the market. It is available during, and unlocked after completing, a certain request.
  - The player can unlock the ability to freely enter and leave (and escape) the dungeon by completing a certain request. This allows easy access to dungeon events. Characters who leave the dungeon without serving their sentence will find their notoriety increased rather than reset.
- **[Social]** Reputation with various factions now has an effect on character social skills. Each social skill now has a faction and an activity related to it. Improving a faction relationship provides a permanent bonus to the social skill. Performing the activity grants experience in the related social skill.

| Social Skill  | Associated Faction | Associated Activity                       |
| ------------- | ------------------ | ----------------------------------------- |
| **Charm**     | Guards             | Maid and barmaid work (tavern and uptown) |
| **Seduction** | Witches            | Prostitution (backstreets and uptown)     |
| **Cunning**   | &mdash; *          | Selling potions (market)                  |

\* *Not yet implemented; the associated faction for cunning is coming in a future update.*

- **[Other]** This update features a few important quality of life improvements.
  - Request progress for most requests is now tracked in the game log (hotkey: `E`). These will remind you of what you need to do next in ongoing requests. You will still have to find out how to start requests on your own!
  - A **Suggested Activities** option has been added to the <u>Perks</u> tab in the character menu (hotkey: `Q`). When you are close to unlocking a perk an entry will be added showing what activities you can perform to unlock that perk. What counts as close? Generally, you must be within three or so instances of any given activity to be considered *close*, but not all activities provide the same amount of growth, and perks can have multiple requirements. In general, this guidance should help give you an idea of what sort of activities you can prioritize to unlock new perks, but it's still a bit of a black box, and you have to be pretty close to unlicking a perk for it to tell you anything.  Further refinements are possible based on player feedback.

### Balance and Progression Adjustments

- **[Social]** Adjustments to social skills have been made across the board. I think they were a little too easy to pass with the new bonuses, so certain penalties were increased. Failure is meant to be interesting and consequential in this game, so too little of it can be a problem. With these increased penalties, I expect players to fail just enough to find themselves in interesting situations, but not enough to be frustrated. Comments on the balance of social skills (and anything else, really) are welcome.

### Performance and Technical Changes

- **[Executable]** Updated NW.js shell to version 0.50.

## v0.4.2 (patch)

> **Version 0.4.2** &ndash; *2020-11-13*
>
> This patch fixes a progression bug in the alchemy subsystem and a dead end when talking to Rais.

### Bug Fixes

- **[Alchemy]** Fixes a progression bug that prevents players from making potions.
- **[Other]** Fixed a dead end in the barracks when talking to Rais after the flashback.

## v0.4.1 (patch)

> **Version 0.4.1** &ndash; *2020-11-12*
>
> This patch fixes a few small issues, adds some new content, and brings the previous update, **Homestead**, to the free edition.

### Bug Fixes

- **[Activities]** Saved games with out-of-bounds expedition party levels will be patched by this game version.
- **[Other]** Fixed some typos and other errors.

### Content

- **[Other]**  Some minor content additions.
  - Some new event content added. This patch also includes some editorial and content changes to existing events.
  - You can now visit Carol and Gray in the backstreets. Doing so will help deepen your relationship with them over time.
- **[Art]** New CGs have been added to a few non-encounter events.

### Balance and Progression Adjustments

- **[Social]** The player character will now earn experience toward certain social skills for performing certain activities.
  - The player character will earn seduction experience for prostitution (backstreets and uptown).
  - The player character will earn charm experience for barmaid (tavern) and maid (uptown) work.
  - The player character will earn cunning experience for selling potions (market).
- **[Activities]** Expedition party member level cap has been raised from 3 to 5. Similarly, expedition loot tables have been expanded and reworked. Expedition rank cap (6) remains unchanged.

### Performance and Technical Changes

- **[General]** Several engine improvements.
  - Stability and performance improvements.
  - Important fixes to the audio subsystem.
  - Reworks and fixes to the encounter system to help it better support new encounter types.
- **[Executable]** Updated NW.js shell to version 0.49.

## v0.4.0 (update)

> **Version 0.4.0** &ndash; **Homestead** &ndash; *2020-11-05*
>
> After successfully aiding the noblewoman Carol Baskin and reuniting her with her beau, Gray, the protagonist finally has the chance to get the aid she needs from the guards to get her revenge. But before she can convince anyone to help her, she'll need to come to terms with just what happened in the past that brought her the city for revenge.
>
> The update completes chapter 1. Also included are a new area, new requests, new potions, new events, new character progression, and more!

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/040-homestead.png" style="margin:auto;">
</details>

### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>To unlock the barracks, simply continue the story.</li>
        <li>When playing through the flashback, experiencing a certain number of events triggers the story to move on, so it's possible to miss certain scenes and events.</li>
        <li>Don't worry: you can perform lewd acts in the flashback!</li>
        <li>You will need to make a sleeping potion for one request. This is a fairly high-tier potion, so may require a bit of grinding.</li>
        <li>There could be "interesting" ramifications if you have sex in front of Priscilla.</li>
        <li>How you interact with Priscilla during a certain request may allow you to upgrade (or downgrade) your relationship with her!</li>
        <li>If you need more tips or want to find out what you might be missing, ask on <a href="https://discord.gg/7BYqxYy" target="_blank">the discord server</a>.</li>
    </ul>
</details>

### Bug Fixes

- **[Activities]** Fixed loot issue in expedition system.
- **[Other]** Fixed several minor issues and typos.

### Content

- **[Story]** Chapter 1 is finished! This update finally finishes and closes out chapter 1.
- **[Requests]** Two new requests have been added.
  - Once the barracks are unlocked, talk to a guard to start one.
  - After meeting certain conditions, head to the forest to find a familiar face.
- **[Other]** Some new events have been added to certain playlists.
- **[Encounters]** Creampies (for all types of sex) have been added to the encounter system. They occur randomly right now. Some elements of player control may be added in the future. More variants may also be added.

### Additions and Enhancements

- **[Alchemy]** Five new potions.
  - **Magnetism Potion +**, **Appeal Potion +** and **Clever Potion +** are upgraded versions of the **Magnetism Potion**, **Appeal Potion** and **Clever Potion** potions and can be used to buff social stats to a much greater degree.
  - The **Gross Flask** and **Cleaning Flask** can be used to manipulate player hygiene levels.
- **[Advancement]** Several (~10) new traits.
  - A new line of traits for increasing the pay of "service" work (that is barmaid and maid work).
  - A new line of traits for increasing the effectiveness of feeding on breast milk.
  - The two new special traits for completing the requests in this update, as you'd expect.
- **[Activities]** A new area, the guard barracks, has been added. It doesn't have much in the way of activities right now, but will be expanded significantly in the future.
  - There's a lot players can do in a new temporary area as well. If you feel like you miss some stuff here, it will be possible to return later in the game, so don't worry too much.

### Balance and Progression Adjustments

- Several balance changes have been implemented in this update. Read about the specifics of&mdash;and the reasoning for &mdash;these changes [in this post](https://www.patreon.com/posts/how-im-your-43270303).
- **[Alchemy]** Several adjustments.
  - Gem dust purchase price reduced.
  - Gem dust drops in gathering activity have been increased.
  - The drop rate from expeditions for spring water, herbs, ale, and gem dust have all been increased. 
- **[Economy]** Several adjustments.
  - Maid work payment has been increased.
  - Prostitution payment in uptown has been reduced.
  - Sale prices for lower tier potions have been increased across the board.
  - Traits for potion sales and prostitution have been reworked and rebalanced.

## v0.3.4 (patch)

> **Version 0.3.4** &ndash; *2020-10-15*
>
> Hotfix for yet another bug, this time in the prologue in the story.

### Bug Fixes

- **[Story]** Fixed a bug where selling potions using the special offers (selling nude/offering favors) did not count toward story progression.

## v0.3.3 (patch)

> **Version 0.3.3** &ndash; *2020-10-14*
>
> Hotfix for another dead end.

### Bug Fixes

- **[Other]** Fixed dead end in maid work.

## v0.3.2 (patch)

> **Version 0.3.2** &ndash; *2020-10-13*
>
> Hotfix for dead end in the tavern.

### Bug Fixes

- **[Other]** Fixed possible dead end in tavern when speaking to Roland at a specific point in the game.

## v0.3.1 (patch)

>  **Version 0.3.1** &ndash; *2020-10-13*
>
> The one that brings the **Honest Work** update to the masses!
>
> Also includes some new content, including a new request, and some bug fixes and editorial changes and corrections. The new content added here is actually fairly significant, so patrons may want to check it out even if they've finished the update!

### Bug Fixes

- **[Interface]** The *breast cancer awareness month* notification should display properly again after being missing from v0.3.0. Players now have the option to dismiss it as well, which should permanently remove it.
- **[Other]** Minor fixes and corrections.

### Content

- **[Events]** Added some new events.
  - New events have been added to uptown exploration, the stocks, and some other places.
  - Some new social reactions have been added.
- **[Other]** 
  - Added a new request. This one was planned for v0.3.0 but was incomplete at launch, but it's in the game now :). You must meet certain conditions to unlock it. Find it in the backstreets when you meet said conditions.
  - Some minor editorial changes.

### Additions and Enhancements

- **[Advancement]** A new special trait is available by completing the new request.
- **[Activities]** The player character can now freely enter and leave the stocks after meeting certain conditions. This allows the player to view stocks-related events at will, which also grant sexual experience. Note that the player character can only freely leave the stocks if she voluntarily entered them; if she was placed in them after being arrested, she will have to serve her entire sentence.

### Performance and Technical Changes

- **[General]** Minor performance improvements and engine adjustments in preparation for third-party event support.

### Other

- **[Meta]** Made updates and adjustments to the changelog.

## v0.3.0 (update)

>  **Version 0.3.0** &ndash; **Honest Work** &ndash; *2020-10-06*
>
> This update adds a bunch of new ways to make and spend money. You can work for Roland at the tavern as a barmaid, train to be a servant in uptown and work as a maid, and hire your own servants to go on expeditions to collect ingredients for you. Also added is a new potion to induce lactation in both the player and female NPCs, and a bunch of new lactation-related erotic content.
>
> Also included is new story content and requests, new events and activities, and new character progression, potions, abilities, and more.

<details title="Click to reveal.">
	<summary>Key Art (NSFW)</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/030-honest-work.png" style="margin:auto;">
</details>

### Hints and Tips

**Note:** It's recommended that you play the game at least a bit before reading these hints. While some content can be permanently missed, and some choices lock you out of certain paths, a big part of the fun of a non-linear game is making choices blind.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>To unlock the barmaid activity, talk to Roland at the tavern. This activity can be unlocked any time.</li>
        <li>To unlock the maid academy, continue the story.</li>
        <li>Maid work events will play out differently based on the player character's skill as a maid. Her skill is determined by how many courses you complete without cheating.</li>
        <li>The <b>Lactical</b> is a fairly high-level potion, so may take some grinding to unlock if you are behind on alchemy unlocks.</li>
        <li>Talking to Priscilla while lactating starts a request that allows you to unlock <b>Milk Maiden</b>.</li>
        <li><b>Milk Maiden</b> allows the player character to use lactation in <b>Potion Magic</b>.</li>
        <li>There are three major paths through the Wimbol ordeal. Two paths may be selected without any work upfront. The third path is a bit more hidden.</li>
        <li>You can unlock the third path by triggering two specific uptown events. Hint: these events both deal with the same character.</li>
        <li>You can explore Wimbol's manor to uncover secrets that will allow you to complete your plans much, much more easily. Finding these secrets may take time, and may take multiple tries, especially if certain social skills are low.</li>
        <li>There are a couple main ways that the Wimbol portion of the main story can end, and which result you get will have large short-term and long-term consequences.</li>
        <li>Unlike most previous challenges, it is possible to fail in your interactions with Wimbol. Failure will have negative consequences, but it isn't the end! The story may take some interesting turns from failure!</li>
        <li>Some of the requests this time around also have multiple, mutually-exclusive paths and endings. From this update on, it will no longer be possible to see all game content with one file, and more events and scenes will be permanently miss-able!</li>
        <li>If you need more tips or want to find out what you might be missing, ask on <a href="https://discord.gg/7BYqxYy" target="_blank">the discord server</a>.</li>
    </ul>
</details>

### Bugs Fixes

- **[Advancement]** Fixed the trait <u>Preservation</u>. It should now proc at the intended rate.
- **[Social]** Fixed an exploit allowing players to easily escape guards.
- **[Other]** Internal improvements and minor fixes.
- **[Typos]** Fixed several typos.

### Content

- **[Story]** The second part of chapter 1 has been implemented and can be played. I plan for the next update to close this chapter out.
- **[Other]** Added two new requests.
  - Talk to Priscilla after meeting certain conditions to get a second request from her.
  - Talk to a noble who's losing his shit in the maid academy after completing maid training.
- **[Events]** Many new events were added.
  - Many one-time story and side events have been added.
  - Some of the new activities have new events, most notably maid work in uptown.
  - Some old events were given new branches and options. Many of these are in relation to the new lactation options.
  - Some areas have new events.
- **[Encounters]** Some new encounter scenes were added.

### Additions and Enhancements

- **[Alchemy]** Six new potions have been added, as well as a new ingredient.
  - **Calming Balm +** and **Heat Liquor +** are upgraded versions of the **Calming Balm** and **Heat Liquor** potions and can be used to manipulate lust.
  - The **Lactical** can be used to induce lactation in the player or NPC women. To stop lactating, take the potion again.
  - The **Filthy Flask** can be used to instantly set the player character's hygiene level, like with the **Moist Flask** and **Dirty Flask** before it.
  - The **Succubus Snack** is essentially an upgrade to the **Preserved Sperm**, and is more filling. The <u>Preservation</u> trait will apply to them as well.
  - The **Sleeping Potion** can be used to sedate NPCs. You can use this to get past certain obstacles or even to rob characters if you're so inclined. No effect on other demons.
  - Breast milk has been added as a potion ingredient. You can also buy breast milk at the market if you want to avoid lactation content. It can also be earned from <u>expeditions</u> using witches of a certain level (see below).
- **[Advancement]** New advancement opportunities have been added.
  - Lesbian experience and fetish growth has been added.
  - Three new special traits that can be earned by completing requests, two of which are mutually exclusive based on how you complete a certain request.
  - Two new perks to unlock, both related to lesbian/bisexual content.
- **[Activities]** 
  - The player can now use **Lactical** potions to induce lactation in themselves and NPC women. In the latter case they may feed off the generated breast milk. The player character can now lactate, similar to how they urinate. Other small activities and actions related to lactation have also been added.
    - The player can lactate at home, in public (backstreets or uptown), or in the dungeon. The more public options may require exhibitionism-related perks.
  - The player character can unlock the ability to use their breast milk directly with the magic goblet. Doing so has a chance to randomly cause a sleeping or love potion effect. It will also sometimes have no effect at all. In general, the player will only have this option when using love potions or sleeping potions would be an option.
  - The player character can now hire servants to send on <u>expeditions</u> to collect ingredients. Each type of servant has a set of specific ingredients they can collect. Servants can be leveled up to improve the amount of ingredients they gather, but they will cost more as well.
  - The player character can work for Roland at the tavern as a barmaid. This is a non-lewd job, and it pays a bit worse than backstreets prostitution but doesn't increase notoriety or depravity.
  - The player character can also work as a maid in uptown after completing the training course at the maid academy (you'll do this as part of the story). Working as a maid has many more opportunities for lewd stuff than barmaid work, and much more detailed events. How good a maid the player character is will be based on how they did in the course, and their skill and performance will determine their options in these events.
  - With the appropriate perks, the player character can now set up shop nude and/or offer oral sex to customers when selling potions through the new <u>special offers</u> system. Doing so dramatically increases sell-through and selling prices, but there's a chance that she will get in trouble with the law. New special offers may become available in later updates if people like the system.
- **[Other]** You may now change the player character's alias from the character menu at any time. Click the pencil icon next to the name. This change may not take effect completely until you change screens. **Known issue:** This actually allows you to choose some reserved names that are unavailable in normal character creation. This will probably eventually be patched, but it is very low on my to-do list.

### Balance and Progression Adjustments

- **[Alchemy]** Prices and ingredients have been adjusted for some potions, largely to help selling potions feel a bit more worth it compared to prostitution. 
- That's it, not a lot of balance changes this time around. Feel like the game is pretty close to where I want it to be, balance-wise, but I'm open to feedback.

### Performance and Technical Changes

- **[Executable]** Updated NW.js shell to version 0.48.

## v0.2.2 (patch)

>  **Version 0.2.2** &ndash; *2020-09-10*
>
> The one that adds some CGs and adjusts the game balance a bit.

### Bug Fixes

- **[Other]** Internal improvements.

### Content

- **[Art]** Art has been added to lesbian scenes.

### Balance and Progression Adjustments

- **[Economy]** Some prices have been adjusted.
  - The minimum chance for selling a potion has been increased.
  - Prostitution in uptown is *slightly* less lucrative (about an 11% overall decrease). It's still very significantly more lucrative than the backstreets.
- **[Social]** Social event odds reduced slightly. You should encounter social events less often in general. There should be less occurrences of back-to-back social events as well.
- **[Reputation]** Notoriety now decays very slowly over time.

## v0.2.1 (patch)

>  **Version 0.2.1** &ndash; *2020-09-02*
>
> The one that brings the Consequences update to the free edition, along with a few minor fixes.

### Bug Fixes

- **[Story]** 
  - The player portrait in the flashback should now always be clean.
  - The skip prologue option should now actually work.
- **[Other]** Several dozen minor stability improvements and bug fixes.
- **[Typos]** Several typos and grammar errors fixed.

### Content

- **[Events]** Made some content revisions to improve the readability and eroticism of certain events.

### Additions and Enhancements

- **[Interface]** The following options have been added to the content shield system:
  - Degradation/humiliation content. This is a bit imprecise, but generally degrading behavior that isn't exhibitionist (or otherwise better described by another option) is hidden by this setting.
  - Group sex, which includes most sexual content with more than two participants.
  - <u>Dev note:</u> The content shield is still in beta, so it's possible I've missed instances of certain content, so be prepared for that possibility.

### Balance and Progression Adjustments

- **[Social]** Social skills should now succeed more often across the board.
  - Priscilla's <u>enchantment</u> ability now has a stronger effect.

### Performance and Technical Changes

- **[Executable]** The executable version of the game will now automatically check for updates and alert the user if it finds one, linking to the appropriate edition's Itch page. Right now this requires manual updating of the JSONP data on my personal website meaning it could be error prone, but I hope to automate it eventually.

### Other

- Updated about menu. Made several wording edits.
  - Added legal disclaimers.
  - Removed old music credit.
  - Added <u>$10 Patrons</u> thanks section.

## v0.2.0 (update)

>  **Version 0.2.0** &ndash; **Consequences** &ndash; *2020-08-27*
>
> This update adds several new reactions and consequences to the world in reaction to a variety of player statuses and interactions: things like characters reacting more strongly to the player's hygiene, dress, and reputation as well as law-enforcement hassling the player or even trying to arrest them.
>
> Also included is new story content, new areas, new events, new actions, new activities, new scenes, new character progression, new abilities, new artwork, and even a new musical score.

<details title="Click to reveal.">
	<summary>Key Art</summary>
    <img src="https://outsiderartisan.neocities.org/succubus-stories/promo/update/020-consequences.png" style="margin:auto;">
</details>


### Hints and Tips

**Note:** It's recommended that you play the game a bit before reading these hints. Very little content can be missed, so you can come here  later when you're stumped or to see if you missed anything.

<details title="Click to reveal.">
    <summary>Hints (Spoilers!)</summary>
    <ul>
        <li>To get to uptown, complete the prologue story.</li>
        <li>If you're stuck in the prologue because of Burke, you should now be able to continue.</li>
        <li>Priscilla can be found at night in uptown. Follow her around to complete her request and unlock <b>Potion Magic</b>.</li>
        <li><b>Potion Magic</b> opens up new opportunities in new and old events. These new opportunities are different based on the gender of the target.</li>
        <li>You can get alternate scenes by showing up naked to various Priscilla events. Do it at certain points to unlock a special route with her.</li>
        <li>Talk to Roland in the tavern to start a request that unlocks the <b>Glory Hole</b> activity.</li>
        <li>When you need to get arrested (you will need to as part of the story), hang out in uptown. Perform prostitution there in particular to get notorious fast. Guards will only hassle you in the market and uptown (right now, more future areas will also feature guards), but the market closes and can deny you entry so it's less convenient to hang there. You can only be arrested after the prologue ends.</li>
        <li>This is really a hint for the previous version, but don't forget that you can use <b>Moist Flasks</b> to (mostly) clean yourself quickly without taking up any time. This can be useful when you need to be in a clean(ish) state to interact with characters.</li>
    </ul>
</details>

### Bug Fixes

- **[Story]** It is no longer possible to get stuck and run out of options when dealing with Burke.
- **[Other]** Several dozen minor stability improvements and bug fixes.
- **[Typos]** Several typos and grammar errors fixed.

### Content

- **[Story]** The first part of chapter 1 has been implemented, and a significant portion of it can be played.
- **[Other]** Introduced the game's first two requests (i.e., side quests). One can be undertaken by talking to Roland after completing the prologue, another by visiting uptown at night. Each one unlocks special traits and a new activity. One also unlocks a powerful new special ability.
- **[Art]** Art has been added to solo urination and solo masturbation events. I like the latter a lot more than the former, so I may take another crack at the urination art sometime in the future. I think it's the expressions and the more relaxed-looking poses of the masturbation scenes that really made them. I plan to add art to certain other kinds of events slowly as time goes on. The next set of scenes I'll tackle will be probably be lesbian scenes. No ETA or promises on that yet, though.
- **[Areas]** Uptown has been added. While it largely has similar activities to the backstreets, it features a completely new set of events and new activities to further differentiate it will be added in future updates, though the two areas will remain somewhat similar (like the lake and forest, for example).
  - The dungeon serves as a small area with several options and activities, but it can not be navigated to normally&mdash;you must be arrested to visit it (at least at first). You will need to visit it at least once as part of the chapter 1 story.
- **[Events]** Many new events were added.
  - Many one-time story and side events have been added. (<u>Hint:</u> Try messing with Priscilla. Get naked a lot around her and see what happens!)
  - Many old events have been updated with new options (most new options are related to a new ability unlocked in a request).
  - Some old areas have new events.
  - Uptown has several all-new events.
  - New events now occur at random based on character status and reputation while traveling around.
  - The new notoriety related areas/activities, the dungeon and the stocks, both have several random events.
  - Several generic lesbian sex scenes were added. These are not encounters, instead they're treated like small, one-page events. You can usually trigger them by using a certain new ability in combination with a certain type of potion whenever you interact with a female character, though there are other ways.
- **[Encounters]** New encounter scenes were added.

### Balance and Progression Adjustments

- **[Alchemy]** 
  - Ingredient requirements have been adjusted. Most potions require less ale and gem dust. Some potions that required gem dust now require other, more easily farmable ingredients, such as ale or urine.
  - Potion sale prices have been adjusted. In general selling higher-ranked potions is now much more lucrative. Lower-ranked potions are about the same.
  - Recipe progression has been flattened across tiers. Experience values were not changed, but were capped lower, so some users may have more potions unlocked than before (no one will lose potion unlocks, however).
- **[Advancement]**
  - Traits are now earned slightly (~16%) faster. This is now about twice the rate of gain as in v0.1.1. Further adjustments may be necessary.
  - Perk requirements have been adjusted. If you have already unlocked perks in the previous version, you will not un-unlock them or anything, however.
- **[Economy]** Ingredient prices have been adjusted. Base prostitution payment has been adjusted downward but performance has a larger impact on the final price. This should be a net increase in funds gained from prostitution, but also give it a wider swing.
- **[Encounter]** Partners now last slightly longer during sexual encounters. Since most encounters can be skipped, this shouldn't make the game significantly more grindy, but it should provide more materials for alchemy and make it easier to get good performance values when needed.
  - Previous changes to some partners' starting lust lower limit has been reverted to pre 0.1.4 numbers. This should make getting higher performance scores easier.
- **[Social]** Penalties to social skills from hunger and lust have been reduced. They can be further reduced with the new enchantment feature. This feature can be unlocked by making a certain character like you more.

### Additions and Enhancements

- **[Interface]** The "content shield" system has been introduced in beta. Only urination and lesbian content can be hidden for right now; see the config menu to set it up and try it out. If the system works well and people like it, I will add more content settings.
- **[Alchemy]** Six new potions across three ranks were added. Make other potions to rank up and unlock new recipes.
  - **Merchant Salve** - Dramatically increases odds of a critical sale occurring while selling potions. Temporary effect. (<u>Dev note:</u> When a critical sale triggers, it is sold for twice the normal price. By default there is a roughly 5-10% chance of this occurring. This is separate from the normal sale chance, meaning there's a 5-10% chance of critically selling a potion, and if it is not critically sold, there is then a chance to normally sell it. This potion increases the chance of a critical sale occurring to 30-35%, so a massive bonus.)
  - **Piss Liquor** - Makes a character always need to urinate. May also be useful to collect urine... Temporary effect.
  - **Dirty Flask** - Immediately grants the hygiene status "dirty." (<u>Dev note:</u> Did you realize that with the **Moist Flask** you can clean yourself quickly without needing to go and bathe? This potion can also be used to set you level of hygiene.)
  - **Sensuous Perfume** - Increases the odds of social events occurring. Temporary effect. (<u>Dev note:</u> A "social event" is an event triggered by status, reputation, or notoriety. See below.)
  - **Dull Perfume** - Decreases the odds of social events occurring. Temporary effect.
  - **Parity Potion** - Feeding also sates lust. Temporary effect.
  - In addition to new potions, completing one of the requests allows the player character to use a powerful new ability related to alchemy. If you fail to complete this request, you can also gain this ability later in the story in a future version by different means.
  - *[Patron Edition Only] [Cheats]* You can now use potions even when you don't have them, in certain situations, when cheats are turned on. For example, if you have the option to use a love potion, and you have one on you, it will be used as normal. If you don't have one and cheats are on, the option will feature a **Cheat** label and you can select the option anyway, as if you did have a love potion. This option will only be available in specific (usually non-story, non-request) events.
- **[Advancement]** New advancement opportunities have been added.
  - A new line of traits related to selling potions; three total.
  - A new line of traits related to prostitution; four total.
  - Two new special traits that can be earned by completing requests.
  - Three new higher-level perks to unlock. Two have associated actions, while the actions for the third are not yet present in the game, but should come in subsequent versions.
- **[Activities]** 
  - The player character can now eat semen from off her body for sustenance. Has a diminished effect compared to feeding normally. Requires one of several combinations of perks and hunger levels to perform, and can eventually become more or less a permanent option. Doing this cleans the character's body by a moderate amount, so it cannot be done indefinitely.
  - Uptown features a higher-risk, higher-reward version of prostitution. A new character to interact with can also be found in uptown at night.
  - A new activity is added to the tavern after completing a certain request. 
  - Players can serve out a sentence after being arrested in the stocks.
  - Players can serve out a sentence after being arrested in the dungeon, which is sort of a mix between an activity and an area.
- **[Encounter]** Two new actions have been added to oral sex scenes. Both require certain perks to unlock.
- **[Social]** All new reputation and status-based random events have been added. These have a chance to trigger automatically when entering any area. They are on a cooldown to prevent happening too frequently.
  - Notoriety in specific can now cause the player character to be hassled or even arrested by the guards. These interactions can lead to the player serving a sentence in the dungeon or stocks.
  - The new sensuous and dull potions can be used to increase or decrease the rate at which these events occur.
- **[Media]** Most background music has been replaced.

## v0.1.5 (patch)

>  **Version 0.1.5** &ndash; *2020-08-08*
>
> The one that fixes the grind.

### Bug Fixes

- **[Encounter]** Fixed minor bug that made it possible for partner stamina to exceed normal limits.

### Balance and Progression Adjustments

- **[Alchemy]** Reduced potion ingredient requirements. (<u>Dev note</u>: changes in the last patch made bodily fluids, esp. semen, much harder to gain in large quantities. This patch should fix that.)

## v0.1.4 (patch)

>  **Version 0.1.4** &ndash; *2020-08-07*
>
> The one that's a bit more world-weary. Mostly adjustments to make the encounter system more palatable.

### Bug Fixes

- **[Typos]** Fixed instances where default name was used instead of player-chosen name.

### Content

- **[Other]** The ring event in the backstreets now pays out a fairly large sum of money in addition to a secret effect later in the game.

### Balance and Progression Adjustments

- **[Advancement]** Traits are now earned 25% faster.
- **[Needs]** Base hunger rate reduced by 33%. The metabolism line of traits effects were slightly reduced as a result.
- **[Encounter]** To make the encounter system less time consuming the following changes were made:
  - Partner stamina has been reduced. Most partners should now tire out after 2 or 3 orgasms, with it very rarely requiring 4 and never requiring more than 5.
  - Partner lust is now weighted to be higher at the start of every encounter. Lust determines the effectiveness of actions in building the orgasm meter, so this means that making partners orgasm should now take less time across the board. This may make higher performance ratings more challenging, however.

### Additions and Enhancements

- **[Encounter]** To make the encounter system more engaging, the following changes were made:
  - Some encounters can be skipped. Only encounters where performance has no bearing on the outcome (excluding prostitution), where it is not part of a story or unique event, where the partner is not a unique character, and it is not the first encounter of its type to be played in the current save. Skipping encounters currently always results in a C performance, but may be weighted based on character skill and perks in a future version.
  - Performance descriptions have been changed. Low rankings now damn you with faint praise instead of suggesting the man who just orgasmed several times was unsatisfied.

## v0.1.3 (release)

>  **Version 0.1.3** &ndash; **Initial Release** &ndash; *2020-08-06*
>
> The first release! Yay!

### Bug Fixes

- **[Story]** Fixed several bugs in story content.
- **[Advancement]** Some perks intended to be available were cut off by the cap. Loosened the caps a bit (see below).
- **[Economy]** Fixed rent bug that caused rent to occasionally be doubled.
- **[Typos]** Fixed several typos and grammatical errors.

### Balance and Progression Adjustments

- **[Alchemy]** Lower material requirements for world materials.
- **[Advancement]** 
  - Adjusted some perks.
  - Loosened fetish point caps.
- **[Economy]** Increased maximum discount for cauldron. 

## v0.1.2 (beta)

>  **Version 0.1.2** &ndash; *2020-07-25*
>
> The one that, at long last, is a candidate for launch!

### Bug Fixes

- **[Interface]** Male dialogue lines should now be easier to read in dark mode. Further improvement of dark mode readability will be coming in a future version. For now, users with sight issues may have to avoid using the dark mode.
- **[Alchemy]** Fixed temporary potions; effects are now properly expired when intended.
- **[Activities]** 
  - Certain scenes where time wasn't passing have been fixed.
  - Emergency behaviors that occur in public are now properly described as occurring in public.
- **[Misc]** Fixed a bug in the image preload system.

### Content

* **[Story]** 
  * The game's prologue chapter is now complete and fully implemented.
  * When the story content is complete, the game will now check various rare world events and give the player hints if they were not completed.
  * Hooks for Chapter 1 have been added.
  * The cauldron can no longer be stolen. (<u>Dev note</u>: I made this change to force players to actually engage with game systems and activities. To compensate, the price of the cauldron has been significantly reduced.)
* **[Other]** New events added to several locations to fill out world activities.

### Balance and Progression Adjustments

- **[Alchemy]** Complete rebalance of ingredient requirements: potions now have steeper material requirements in regard to bodily fluids and lower requirements in regard to world materials.
- **[Advancement]**
  - Adjusted perk progression rates and attempted to spread perks out more.
  - Traits should now unlock ~20% faster. Progression in v0.1.1 seemed a bit *too* slow.
  - Recipe progression has been capped much lower than originally intended to push buggy potions into later releases. Recipe progression has been adjusted to be more of a curve, with early recipes now coming faster and later recipes coming slower. Some game materials, like guides, may feature outdated potion/recipe lists&mdash;I'll update these when I can.
  - The caps to experience and fetish growth have been slightly tightened. I don't want players of this version unlocking everything in the next version the second they load up the game, but some reward for playing a lot should still be in place so players can hit the ground running, so to speak.
- **[Economy]**
  - Prices and income have been adjusted across the game.
  - The cauldron is now notably less expensive, but is no longer discounted at the same rate as other items.
  - Performance in encounters now contributes more meaningfully to payment in prostitution.
  - Prostitution in general should be more financially rewarding.
  - Economy (difficulty) setting adjustments have been altered.
  - Potion prices have been adjusted.
- **[Encounter]** Adjusted critical chance. Player traits for the relevant sex act now influences chances of scoring a critical on a partner.

### Additions and Enhancements

- **[Interface]** 
  - Potions of the same type now stack in the inventory interface.
  - Added tester credits to game's about menu.
  - Normalized the appearance of modals.
  - Altered loading screen.
  - Improved UI and branding.
  - Dreams are now rendered in a special style for the sake of clarity.
- **[Encounter]** Finishing touches.
  - Added many (~50) new scenes, bringing the total scenes to more than 160 (not counting randomized descriptive text).
  - Performance is now reported to the player and graded on a scale of (from best to worst) S, A+, A, B, C, D, F.
- **[Economy]** Player must now pay a weekly rent payment of 30G or go into debt, a status which can have serious consequences.
- **[Activities]** Players can now sell potions at the market after they acquire a vendor's license (getting one is part of the prologue storyline and unmissable).
- **[Social]** The reputation system has been fully implemented across all events and activities, however, most consequences are not yet implemented. Consequences, particularly legal and social consequences, will come in a future update, so you may still want to heed warnings about getting in trouble!

### Performance and Technical Changes

* **[General]** Performance improvements.
* **[Engine]** Made several engine improvements and adjustments.
  * The compiler now compresses game data to keep the file small (achieved around 50% data compression) to improve loading and downloading speeds. Decompression is handled by the engine at runtime, which is significantly faster than the larger download on almost all devices at even high internet speeds.
  * Unused code was removed from the engine, particularly unused macros, but also some legacy code.
* **[Web]** Made caching and preloading of assets more aggressive. More is preloaded by default. 
  * (Dev note: I am not confident in or happy with the performance of the web version of the game, and I'm nearing the limit of my ability to optimize it. If it gets any worse, I may have to stop supporting the web version entirely as I think it will give people a highly negative first impression of the game, even for users with fairly high download speeds.)

## v0.1.1 (beta) 

>  **Version 0.1.1** &ndash; *2020-06-16*
>
> The one where I try to fix all my dumdum brain dums. And move the traits interface.

### Bug Fixes

- **[Alchemy]** Several fixes and adjustments to the alchemy subsystem.
  - Fixed bug where drink links were not being properly scoped such that every drink link in the potion inventory always used the last potion on the list.
  - Fixed a bug in the buff potion due to a typo.
  - Fixed several bugs in the potion list due to typos and off-by-1 errors.
  - Addressed several other bugs and inconsistencies.

- **[Advancement]** Fixed bugs in trait and alchemy advancement.
  - Trait advancement notifications should now display properly.
  - Fixed bug where alchemy advancement did not occur.
- **[Activities]** Fixed market discounts and chaining bonuses.
  - Fixed issue where notice about losing a discount was reversed, meaning it appeared when the player didn't have a discount and did not appear when they did.
  - Chaining bonuses were bugged previously and are now properly applied.
- **[Typos]** Fixed several typos and grammar errors.

### Balance and Progression Adjustments

- **[Alchemy]** Reordered some potions on the tier list.
- **[Advancement]** Made several adjustments.
  - Traits are now earned at a consistent rate of 1 per 1000 tp.
  - Perks have been adjusted to provide a more even-keeled progression; still possible to frequently unlock two or more perks at once with long dry spells in between, so it will need further refinement.
  - Significantly decreased the rate at which alchemy recipes are unlocked.
- **[Activities]**
  - Prostitution progression has been overhauled. Players start with the ability to perform oral sex. The "No Shame" perk allows players to engage in anal and vaginal sex. In all of these cases, the player takes the customer somewhere secluded. When the "True Whore" perk is unlocked, the player does not take the customer to a secluded place and unlocks the ability to chain encounters.
  - Prostitution now gives slightly more money based on the requested sex act, with anal costing more than oral and vaginal costing the most.
  - Streaking requirements reduced. Now requires the "Nudist" perk instead of "No Shame".
- **[Misc]** Adjusted bonuses from immigration job selection.

### Additions and Enhancements

- **[Interface]** Made some general improvements to the CSS styling and accessibility.
  - The location list now has labels that display more information when hovered over.
  - The traits interface has been moved from the "Rest" activity at home to the Character menu. The number of available points is now also shown.
  - The status bar now updates in real-time when potions are used and in other similar situations.
  - The character portrait now also updates in real time.
  - Starting a new game now only warns about overwriting your autosave if an autosave exists.
- **[Advancement]** Added a few high-tier perks for later use.
- **[Misc]** Added "testing" flag for game builds.

### Performance and Technical Changes

* **[General]** 
  * Increased idle time on startup load to avoid flashes of un-styled content.
  * General improvements to codebase and performance.

- **[Web]** 
  - SVG icons are now always preloaded, and preload independently of other image assets.
  - Fonts are now preloaded.
- **[Executable]** Reduced compression to improve performance and start-up times, but file size increased by about 10MB across all versions.

## v0.1.0 (beta)

>  **Version 0.1.0** &ndash; **Test Version** &ndash; *2020-06-11*
>
> The one where I get some help testing the progression mechanics.

* **Test Edition** released to gather advancement and performance feedback.