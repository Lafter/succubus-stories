> As of v0.10.1

- Home
- Cottage*
- Backstreets
  - Stocks*
- Tavern
- Market
- Uptown*
  - Maid Academy*
  - Dungeon*
- Castle*
- Barracks*
- Bandit Camp*
- Forest
  - Coven*
- Lake

\* Unlocked by progressing in the game.
