# Game Manual

This is a basic manual and guide for the erotic adventure game, *Succubus Stories*. This guide, as with the game, features graphic adult language and themes, and is intended only for adults over the age of 18.

[You can play the free edition of *Succubus Stories* on itch.io.](https://outsiderartisan.itch.io/succubus-stories-free)

[Access the Patron Edition.](https://outsiderartisan.itch.io/succubus-stories-patron/patreon-access)

[Consider supporting development via Patreon to unlock the patron edition of the game, along with other goodies.](https://www.patreon.com/outsiderartisan)

[Report bugs on the issue tracker.](https://gitlab.com/Lafter/succubus-stories/-/issues/)

[Visit the game's web page.](https://outsiderartisan.neocities.org/succubus-stories)

## Contents

- [Getting Started](#getting-started)  
- [Controls](#controls)  
- [Introduction](#introduction)  
- [Locations and Activities](#locations-and-activities)  
- [Basic Needs](#basic-needs)  
- [Encounters](#encounters)  
- [Problem Solving](#problem-solving)
- [Requests](#requests)    
- [Progression](#progression)  
- [Settings](#settings)  
- [Cheats](#cheats)

## Getting Started

*Succubus Stories* can be played online in any modern web browser (Internet Explorer 9+) with almost any device, or downloaded and run as an executable on Windows systems. In general, the executable version has better performance, and is recommended. The executable version of the game will also support mods in the near future. Saved data can be manually transferred between devices and versions using the `Save to disk` feature.

<img src="https://outsiderartisan.neocities.org/assets/img/store.jpg" style="margin:auto;border-radius:15px">

### Which Version Should I Play

If you want to play on mobile or an unsupported operating system (e.g., Mac or Linux), your only option is to play the browser version, which can be downloaded for offline play, or played on Itch.io on the web. The executable version is recommended for Windows users.

If you need to know whether you're on a 32-bit or 64-bit Windows system, check these guides:

- [Windows: "How Do I Know if I'm Running 32-bit or 64-bit Windows?" &ndash;How-To Geek](https://www.howtogeek.com/howto/21726/how-do-i-know-if-im-running-32-bit-or-64-bit-windows-answers/)

### Controls

The game can be played with mouse or touchscreen alone, but also feature a suite of keyboard controls for users that prefer them. Press and hold the `Shift` key at any time to see which buttons on the screen are mapped to which hotkeys.

- Number keys `1` - `9`, `0`, `-`, and `=` can be used to select one of the main options in any given scenario.
- The number pad's `1` - `9` and `0` may also be used to select the main options. Options mapped to `-` or `=` are not represented on the numpad.
- `Q`, `W`, `E`, `A`, `S`, `D`, `Z`, `X`, and `C` can be used to activate options in the sidebar.
- `Esc` can be used to open or close the sidebar.
- `Esc`, `Q`, or NumPad `.` can be used to close dialog windows if any are open.
- `Shift` can be held to view hotkeys.
- `Ctrl` can be used to toggle text overlays during sexual encounters.
- `K` can be used to skip sexual encounters.
- The arrow keys can be used to scroll.
- `Tab` can be used to move the focus around the page; `Shift + Tab` can be used to move the focus in reverse.
- `Space` or `Enter` can be used to activate focused elements.

## Introduction

In *Succubus Stories* you play as a peasant girl turned succubus, plotting her revenge against the powerful person who ruined her life and stole her family's land. She needs to consume a certain bodily fluid to survive, and has to manage her ravenous lust as she gets closer to her goals.

The game has a central narrative, but the primary goal of the game is to give players a lot to do and a lot to work toward with an open-ended structure where players can progress at their own pace. Players will need to manage their reputation, skills, and money through prostitution, sex, diplomacy, alchemy, and business.

## Locations and Activities

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/side3.jpg" style="max-width:50%;float:right;margin-left:1rem;border-radius:15px">[See List](areas.html)

Most of the game will be spent visiting various in-game locations and performing a variety of both chaste and lewd activities. While some activities can be performed for free, most cost time. At different times, different activities may become available or unavailable in various locations.

In most locations, there is an option you can use (`Hang out`, `Explore`, etc) to experience various repeatable random events. You will also unlock new activities and events as you progress the game's main story, make friends with new characters, ingratiate yourself with various factions, complete requests, and unlock new perks and traits.

As time goes on, the main character becomes hornier and more hungry, and these needs must be sated or the character will be too distracted to perform well in certain activities.

### Home

At her home, the player character can rest to pass time, bathe to stay clean, and masturbate to sate her lust (and gain a small amount of fetish points based on what she fantasizes about). After a certain point in the game, she will also acquire a cauldron that lets her perform alchemy with&hellip;her *leftovers*, we'll say. 

### Backstreets

The backstreets can be a bit dangerous, but since there aren't many guards, it's also a great place for prostitution.

### Tavern

A lot of interesting folks hang out in the tavern. It's also a safer, if less profitable, place to work than the streets.

### Forest

The forest outside town is a great place to collect herbs and other alchemy ingredients. Watch out for bandits and witches, though&hellip;

### Market

Some ingredients can be purchased instead of painstakingly collected. In addition, the player character can sell potions to make a decent income, if she can get her hands on a vendor's license.

### And More

There are several other locations, including many that can be found or unlocked as you play, all with unique activities, characters, and events.

## Basic Needs

The player character has a few basic needs that the player must manage.

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/page3.jpg" style="margin:auto;border-radius:15px">

### Rent

The player character needs to pay her rent every week. The number of days she has until the rent is due is shown in the status bar&mdash;it will start flashing when there are only a couple of days left. Rent costs 10G, and the player character will go and pay it automatically when it's due. If she doesn't have enough money, she'll go into debt. You won't be able to spend any money until the player character has enough money to cover her debt. Remaining in debt for long periods of time or being in an excessive amount of debt can get her in trouble.

### Urination

The player character needs to urinate every so often. At first this can only be done at home, but with certain perks (or during certain events) the player can urinate in other places. Failure to urinate may cause the player to piss themselves. If this happens in public, they could get in trouble.

### Lust

The player character needs to have sex or masturbate frequently, or her lust will go to her head. Having high lust causes the player character to perform much more poorly in various tasks, including her social skills. The player character simply needs to have an orgasm to calm down. Failure to manage her lust may cause the player to drop and start masturbating no matter where she is. If this happens in public, she could get in trouble.

### Hunger

The player character must eat semen to survive. The player character can get semen from a <u>Preserved Sperm</u> potion, a sexual encounter, and many different events. With the appropriate perks, the player character can even eat semen from off her own body or subsist on breast milk from female NPCs. Being hungry causes the player character to perform more poorly in various tasks, including her social skills, and also causes her lust to increase much more rapidly over time. Failure to sate her hunger could cause the player character to pass out, putting her in danger of being robbed.

### Hygiene

The player character gets sweaty, wet, and eventually covered with more and more semen as she has sex. Being covered in semen may cause characters to refuse to talk to her or to treat her differently, and guards may prevent her from going to certain places. You can use certain potions to change the player character's level of filth. She can also bathe at home to clean herself up.

### Nudity

Not exactly a basic need, but the player character can leave the house nude after acquiring a certain perk. Doing so is much like walking around covered in semen, and causes characters to treat her differently, causes guards to hassle her, and could ultimately get her in trouble. Some NPCs may appreciate the view, however&hellip;

## Encounters

The player character will be having a lot of sex to sate her hunger and lust, and to maintain a steady supply of ingredients for alchemy. You can initiate sexual encounters with a variety of characters throughout the game in several different kinds of encounters.

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/page2.jpg" style="margin:auto;border-radius:15px">

During encounters, there are two bars, one representing the player character's pleasure and the other representing her partner's pleasure. At some point during the encounter, after an orgasm, the partner will become too tired to continue. If the player's pleasure is high when this happens, she'll have high lust after the encounter, so she should try to use different actions to prevent that from happening.

Different actions cause different amounts of pleasure to build up. The player can learn new actions or even entirely new types of encounters by earning new perks, and increase the pleasure potential of her actions, and the energy of her partners, by unlocking various traits.

### Performance

The player character's performance during sexual encounters is ranked based on a variety of factors:

- Keeping the number of orgasms each person gets close will help improve the ranking.
- Averaging fewer actions per partner orgasm will improve the ranking.
- Using a variety of actions in a single encounter and varying actions will improve the ranking.
- Certain traits can grant permanent bonuses to performance rankings for certain encounter types.

There are other factors too: partners who are just not that horny at the moment tend to be harder to please, and when the player character is *too* horny, that also makes getting a high rank more challenging as her pleasure bar fills extremely quickly. Some other factors are also considered in the final calculation.

Performance is ranked on the following scale, from best to worst: S, A+, A, B, C, D, F. Performing especially poorly or especially well may have consequences in some encounters. During prostitution, performance is directly tied to the payment received.

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/page9.jpg" style="margin:auto;border-radius:15px">

### Public Sex

Sex in public causes a crowd to gather, and they will react to the sex acts being performed. Sex in public may enhance the pleasure for some partners or for the player character. To have sex in public, you'll need to earn a certain perk.

Public sex is also illegal, and will surely get the attention of the guards if you make a habit of it&hellip;

### Other Sex Scenes

Some events and actions may lead to their own sex scenes that take place outside of the normal encounter system. These are typically referred to as "events" and many are repeatable. These events typically offer a lot of experience or fetish points in very specific categories compared to encounters (which tend to offer less points but in a larger number of categories), and are usually much quicker to complete. If you want to unlock a specific perk or level up a specific fetish, hunting for events may give you a greater return on investment!

#### Example Events and Activities

Forget just fucking, there are tons of other erotic events and activities to engage in! Here are some examples:

- When you unlock certain exhibitionism-related perks, you will be able to engage in *flashing* and *streaking* activities.
- When you unlock the barracks, you may find a request that leads to some plumbing issues, making your *tongue baths* a popular alternative to conventional showers for the hard-working guards.
- The tavern bathroom could be livened up a bit with a *gloryhole*.

## Problem Solving

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/side2.jpg" style="max-width:50%;float:right;margin-left:1rem;border-radius:15px">When you come up against a problem, there will usually be several different approaches to solving it. Talk to characters and visit different locations to find more options if you feel stuck, or try to progress your character some: a few more points in your social skills or a new potion recipe may be the answer.

### Social Skills

The player character can use **charm**, **cunning**, or **seduction** to try get what she wants. Your character's social skills will grow over time from just using them, and you can enhance your social skills using various potions.

If the social skill fails, you usually can't try again, and failing some checks, like failing to seduce a guard, could get you in trouble or have other dramatic effects. However failing at using a social skill actually increases it more than succeeding, so don't be afraid to try!

### Sex

Some characters can be persuaded by sex if the player character has the right perks to perform certain actions or enact certain fetishes, or if they are given certain potions or otherwise seduced. For example, sex in public may persuade a given character, but you'll need the **No Shame** perk to be able to give them what they want. [Performance](#performance) may also be a factor; if the player character doesn't perform adequately and satisfy her partner, she may not get another chance.

### Potions

Some potions, like <u>love potions</u> and <u>suggestion potions</u>, are made to be used on other characters to get what you want. <u>Magnetism potions</u>, <u>appeal potions</u>, and <u>clever potions</u> can be used to reinforce your social skills to improve your chances of success on that front. <u>Heat liquor</u> and <u>calming balm</u> can be used to manipulate the lust levels of some characters as well (including yourself), which may help you reach your goals.

There are dozens of potions in the game, and many can be used on NPCs or on your own character to induce many useful or sexy effects.

### Money

Sometimes money will just solve the problem, but usually only in exorbitant amounts. You can make money by selling potions in the marketplace or via prostitution. Some other opportunities may also present themselves occasionally. Make sure to save 10G for rent!

### Friends and Allies

Based on your decisions, you may make powerful or useful friends that can help you overcome or even flat-out avoid some challenges. You may want to try visiting a friend and seeing if they have any advice or can offer any help if you're having trouble with something.

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/page6.jpg" style="margin:auto;border-radius:15px">

### Status

You can try approaching a problem in a (literal) different way. Sometimes showing up nude, filthy, or even horny can cause different outcomes and lead to new opportunities.

### Dead Ends

There should be no true dead ends in the game's main story, but being unprepared can cost you a great deal, and often far more than just money. When it comes to requests, sometimes there will only be one solution to a problem, like if a character requests a specific potion, but you can always come back later if you aren't able to fulfill their request right at that moment.

## Requests

As you explore, progress the story, and meet new people, sometimes an NPC will have a job for the player character to do. These are **requests**. Requests you agree to take on will appear in your journal (hotkey: `E`) alongside your other objectives.

It is highly recommended you do as many requests as you can, as important gameplay elements, new activities, and sometimes even new areas are unlocked by taking these on. Even if a request doesn't unlock a new activity or area to explore, every single request grants a unique trait, and many of these are quite powerful.

A very small number of requests are required to complete the story; you will be directed to these requests when they are needed for story progression.

## Progression

As the player progresses in the game, the player character will earn new perks, recipes, and traits, as well as improve her social skills.

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/page1.jpg" style="margin:auto;border-radius:15px">

### Perks

[See List](perks.html)

As the player character has encounters, her experience with different types of sex and her fondness for various fetishes will increase, granting her new **perks**. New perks often grant new actions and new possibilities in and out of sexual encounters. For example, a character with the **Nudist** perk can start going outside naked, which can in turn cause other new events and scenes. A character with the **Filthy Mind** perk will unlock an action that allows them to talk dirty to their partners during encounters.

Perks are like the "corruption" meters you see in other games, but less linear&mdash;you progress down multiple "tracks" with different combinations of fetish points and experience unlocking different perks at different times. It is intended that most players will ultimately unlock all or at least the vast majority of available perks, but in very different orders based on how they play.

#### Perks: Fetish Points

You can see your character's affinity for various fetishes and kinks in the Character menu. Fetish points are easiest to gain from events. Some sources of fetish points, like masturbating at home, are capped and cannot help you raise your character's affinity for that fetish after a certain point.

#### Perks: Experience Points

You can see your character's level of experience with various sex acts in the Character menu. Experience is easiest to gain from encounters. Each time you engage in a given type of sex, you gain a little experience in that kind of sex.

### Recipes

[See List](potions.html)

As the player character performs alchemy and makes potions, she'll become more experienced and unlock new recipes, allowing her to make new potions. Potions can have a variety of powerful effects, and can be sold for a lot of money, so making potions can be extremely lucrative and help you overcome a variety of challenges.

Some potions can only be learned by finding their recipes in the world, from completing requests, or by learning them from certain characters.

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/page8.jpg" style="margin:auto;border-radius:15px">

### Traits

[See List](traits.html)

Trait points are earned for most actions, from making potions to having sex to using skills. When the player character earns a certain number of trait points, she'll be able to purchase a new trait. Traits have powerful passive effects like increasing the pleasure the player character gives partners with various sex acts, increasing how many times, on average, a partner can orgasm before being unable to continue, increasing how much sexual fluid partners can generate, how quickly and player character becomes hungry, and more.

#### Traits: Request Reward Traits

Every request you complete will reward you with a new, unique trait for your character. These traits can only be earned in this way, and are given for free, so you don't have to buy them with trait points. Some very powerful and unique traits can be earned this way.

#### Traits : Advanced Traits

Unlocking certain combinations of perks and traits will unlock "advanced" trait lines. Purchasing the first trait in an advanced trait line will allow you to purchase all the rest. Advanced trait lines offer some of the most powerful and unique traits in the game, and are usually built around a certain theme, like being naked.

### Social Skills

Throughout the game, the player character will be given opportunities to attempt to solve problems using her social skills: *charm*, *cunning*, and *seduction*. If she fails, she won't be able to try again, but whether she fails or not, her social skills increase over time from use. Failing actually causes her social skills to advance *faster* than success!

#### Social Skills: Charm

Use charm to talk characters into things. Usually focused on being nice and conventionally persuasive. These are usually the hardest checks to make, but if they succeed you don't have to do anything else.

#### Social Skills: Cunning

Cunning can be used to make bribes, and to make bribes for less money. It is also used when the player character is being deceptive. These checks tend to be easier but they'll cost you a bit of G.

#### Social Skills: Seduction

Seduction usually involves promising sexual favors in return for whatever you're after. Generally, you'll have to have sex with someone to get them to do what you want. The difficulty of these checks is usually somewhere between charm and cunning.

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/page7.jpg" style="margin:auto;border-radius:15px">

### Reputation

As the player character performs actions she will gain a reputation, both for her actions and behavior, but also for sex. Reputation has an impact on how various characters and groups see you, and can open some doors and close others throughout your playthrough.

#### Reputation: Depravity

Depravity increases when the player character does things that are degrading or gross. Some characters will avoid you, look down on you, or otherwise find you repulsive if you have a reputation for being depraved. If not regularly increased, depravity slowly goes down over time as people forget about your latest scandals and new subjects come into the rumor mill.

##### Increasing Depravity

- Urinate and masturbate in public.
- Have sex with gropers and molesters.
- Say "yes" to everything.

##### Decreasing Depravity

- Avoid doing being lewd in public.
- Punish gropers and molesters.
- Say "no" sometimes, especially to things that sound over-the-top or gross.

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/page5.jpg" style="margin:auto;border-radius:15px">

#### Reputation: Skill

Performing well in sex scenes and generally having a lot of sex will improve the player character's reputation for being a skilled lover. Characters may seek out the player character specifically and even go to great lengths to get a shot with her.

Unlike depravity, skill is rather hard to decrease, so generally you don't have to worry about it eroding much.

##### Increasing Skill

- Get good performance ranks in encounters.
- Give partners what they ask for.
- Seduce people.

##### Decreasing Skill

- Get bad performance ranks in encounters.
- Don't give partners what they want.

#### Reputation: Notoriety

When the player character breaks the law or is caught naked or performing indecent acts in public, her notoriety will increase. If her notoriety increases too much, she'll eventually be arrested. Notoriety cannot be decreased unless you are arrested.

##### Increasing Notoriety

- Perform overt lewd acts in public.
- Leave home nude or walk around dirty.
- Steal or otherwise break the law.

##### Decreasing Notoriety

- Your notoriety will reset once you are arrested and punished.

## Settings

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/side1.jpg" style="max-width:50%;float:right;margin-left:1rem;border-radius:15px">The game features a number of configuration options. Click the settings button in the sidebar or press `Z` to open the settings menu.

- **Theme**
  - **Art Style**: Choose between 3D renders, hand-drawn art, or no art at all.
  - **Dark Mode**: The game features a dark theme for users who prefer it.
  - **Font Size**: You can adjust the font size of text in the game.
  - **Text Window Opacity**: Changes the opacity of the text window overlays during encounters. Use to improve readability.
  - **Wide Screen Mode**: For the sake of readability, game text will try to remain in a limited area in the center of the screen. If you're using a wider than normal monitor, or a monitor with a high resolution, this may make the text look too squished; this option can be used to fix this problem.
- **Audio**
  - **Volume**: Adjust the master volume.
  - **Mute All Audio**: Mute all in-game audio.
  - **Mute Music**: Mute only music.
  - **Mute Sound Effects**: Mute only sound effects.
- **Gameplay**
  - **Economy**: Adjusts the in-game economy. Decreasing this slider (moving it to the left) causes the economy to become more favorable toward the player and makes the game easier.
  - **Lesbian Customers**: Set the rate at which female customers appear when performing prostitution. Turning this all the way down (to the left) will prevent female customers from appearing at all. Turning it all the way up will make all customers females.
  - **Prevent Urination Emergencies**: If this setting is on, the player character will never wet herself. You will still gain the ingredient <u>female urine</u> at a fixed rate while playing, as though she did. This allows you to ignore most urination fetish content without missing out on ingredients.
  - **Advancement Notifications**: Controls whether and how often the game will display advancement notifications. By default, these are only shown when a new perk is unlocked.
  - **Tutorials**: Prevents certain pop-up tutorial messages from appearing. Some explanatory game text is still shown.
- **Performance** (Web version only.)
  - **Preload Images**: Controls whether all images will be preloaded. This improves the game's performance and responsiveness, but can dramatically increase the startup load time. Some images are always preloaded, regardless of this setting.
  - **Preload Audio**: Controls whether all audio will be preloaded. This improves the game's performance and responsiveness, but can dramatically increase the startup load time. Some audio is always preloaded, regardless of this setting.
  - **Autosave Frequency**: Determines how often the game will autosave. Autosaves only occur when the player moves between areas or scenes. Having the game autosave very often may cause noticeable hitching or slow screen changes.
- **Content Shield**: The content shield only *blurs* offending content, it does not remove it from the game!
  - **Click to Reveal**: If this setting is on, you can click any blurred text to reveal it. Useful if you're more on the fence about certain fetishes and worried you may not understand what's happening in a given scene.
  - **Watersports**: Turning this on blurs urination, wetting, and watersports content. Combined with the gameplay setting <u>Prevent Urination Emergencies</u>, you can play *almost* without any mention of urine in the game.
  - **Lesbian**: Turning this setting on blurs lesbian content, including groupsex or bisexual content involving multiple female characters. Also blurs futanari/female and futanari/futanari pairings.
  - **Lactation**: Turning this setting on blurs any content having to do with lactation or breast milk.
  - **Degradation**: Turning this setting on blurs more extreme degradation or humiliation content.
  - **Group Sex**: Turning this setting on blurs all group sex content; that is, any sexual scene involving more than two people.
  - **Futanari**: Turning this setting on blurs all futanari (women with penises) content.
  - **Auto-Initiated Sex**: Sometimes NPCs will initiate sexual encounters or contact with the player character. This setting blurs all such content.

- **Patron Options** (Patron edition only!)
  - **Enable Cheats**: Allows you to access the built-in cheats. See [below](#cheats) for a complete list of effects and options.
  - **Disable Rent Payments**: You will no longer have to make any rent payments. The status bar will still count down to when rent would normally be due.
  - **Disable Emergencies**: Shut off time-sensitive emergency behaviors, like passing out from hunger, urinating oneself, etc. Other negative effects, like penalties to social stats, will still apply.
  - **Freeze Lust**: Prevents lust from automatically increasing due to the passage of time. Lust can still change due to other effects, however.
  - **Freeze Hunger**: Prevents hunger from automatically increasing due to the passage of time. Hunger can still change due to other effects, however.

<img src="https://outsiderartisan.neocities.org/assets/img/promo-2d/page4.jpg" style="margin:auto;border-radius:15px">

## Cheats

Cheats are only available in the patron edition of the game. Turn the **Enable Cheats** setting on in the Settings menu, as outlined above, to use cheats. Turning on cheats adds a new option to the player character's home that allows them to cheat by giving themselves money, items, experience, and more. Some other cheat options appear elsewhere in the game, like letting the player use potions they don't have on hand, for example. Generally, cheat options do not appear in story events or requests.

You can also press the <code>\`~</code> key on the keyboard (to the left of the number row, under `Esc` on QWERTY keyboards) to bring up the in-game terminal (this may not work during encounters and in certain other places in the game). Type `-help` into the terminal for help on how to use it. The terminal is mostly for accessing information, but it can also be used to render new passage content&mdash;however doing so has a very high chance of making the game unstable and glitchy.

Cheats in general can cause issues while playing. You can still report bugs, but be sure to mention if you encountered said bugs after cheating, and which cheats were used. It's recommended that you save to a different slot after activating cheats, to avoid losing progress.

<img src="https://outsiderartisan.neocities.org/assets/img/preview.png" style="margin:auto;border-radius:15px">
