> As of v0.1.2

``` mermaid
graph LR
  A[Metabolism 1<br>Reduce hunder decay.] --> AA[Metabolism 2<br>Reduce hunder decay.] --> AAA[Metabolism 3<br>Reduce hunder decay.]
  B[Saleswoman 1<br>Increase selling prices.] --> BB[Saleswoman 2<br>Increase selling prices.] --> BBB[Saleswoman 3<br>Increase selling prices.]
  C[Oral Expert 1<br>Increase oral sex skill.] --> CC[Oral Expert 2<br>Increase oral sex skill.]  --> CCC[Oral Expert 3<br>Increase oral sex skill.]  --> CCCC[Oral Master<br>Increase oral sex skill.]
  D[Vaginal Expert 1<br>Increase vaginal sex skill.] --> DD[Vaginal Expert 2<br>Increase vaginal sex skill.] --> DDD[Vaginal Expert 3<br>Increase vaginal sex skill.] --> DDDD[Vaginal Master<br>Increase vaginal sex skill.]
  E[Anal Expert 1<br>Increase anal sex skill.] --> EE[Anal Expert 2<br>Increase anal sex skill] --> EEE[Anal Expert 3<br>Increase anal sex skill] --> EEEE[Anal Master<br>Increase anal sex skill]
  F[Sex Expert 1<br>Increase all sex skills a small amount.] --> FF[Sex Expert 2<br>Increase all sex skills a small amount.] --> FFF[Sex Expert 3<br>Increase all sex skills a small amount.] --> FFFF[Sex Master<br>Increase all sex skills a small amount.]
  FFF --> J[Sex Maniac 1<br>Having sex of any kind contributes to trait growth.] --> JJ[Sex Maniac 2<br>Having sex of any kind contributes to trait growth.] --> JJJ[Sex Maniac 3<br>Having sex of any kind contributes to trait growth.] --> JJJJ[Sex Maniac 4<br>Having sex of any kind contributes to trait growth.] --> JJJJJ[Master Sex Maniac<br>Having sex of any kind contributes to trait growth.]
  G[Inspire Vigor 1<br>Partners last longer during sex.] -->  GG[Inspire Vigor 2<br>Partners last longer during sex.] -->  GGG[Inspire Vigor 3<br>Partners last longer during sex.]
  H[Semen Boost 1<br>Partners have larger cumshots.] -->  HH[Semen Boost 2<br>Partners have larger cumshots.] -->  HHH[Semen Boost 3<br>Partners have larger cumshots.]
  HH --> K[Semen Saint 1<br>Partner cum shots contribute to trait growth.] --> KK[Semen Saint 2<br>Partner cum shots contribute to trait growth.] --> KKX[Semen Saint 3<br>Partner cum shots contribute to trait growth.] --> KKXK[Master Semen Saint<br>Partner cum shots contribute to trait growth.]
  I[Apothecary 1<br>Learn new recipes faster.] --> II[Apothecary 2<br>Learn new recipes faster.] --> III[Apothecary 3<br>Learn new recipes faster.]
  I --> L[Alchemist 1<br>Making potions contributes to trait growth.] --> LL[Alchemist 2<br>Making potions contributes to trait growth.] --> LLL[Master Alchemist<br>Making potions contributes to trait growth.]
```
