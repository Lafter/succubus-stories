# Issues

You may report issues [here](https://gitlab.com/Lafter/succubus-stories/issues/new). An issue can be any of the following:

- A programming error, unexpected outcome, or other bug or glitch found in the game (label: `bug`).  
- A typo, grammar error, or wording issue in the game (label: `typo`).  
- An error in **this** document, or any of the documentation in this repository, including typos, broken links, grammar errors, etc (label: `documentation`).  
- An error on the game's webpage, itch.io page, Patreon page, or any other official page, including typos, broken links, grammar errors, etc (label: `press`).  
- A suggestion to improve or enhance the game, such as a new feature, improved UI, improved accessiblity, etc (label: `enhancement`).

When you create an issue, use an appropriate label as outlined above. Do not apply priorities (the labels like `p1`, `p2`, etc) to your issue. Other issues are possible, so if none of the labels apply, don't worry about it, it's still welcome here.

## Writing a Good Issue

When you add an issue, you are **contributing** to the project. Please read and follow the [contributing guidelines](../CONTRIBUTING.html).

When you add an issue, please be specific. For example, try to detail exactly where you encountered a bug in the game, and what you were doing when it happened. If it's a typo in the documentation or the website, be specific and provide URLs.

Please avoid copy and pasting significant amounts of game text here. To the extent possible, try to avoid explicit sexual depictions in your issues, whether from the game or otherwise, as these may cause issues with GitLab.

## Additional Info for Bugs

If possible, please save your game to disk and attach the save file to your issue if you encounter an in-game bug. This will help me reproduce any problems you report. Please also mention whether you are using the patron or free version, and what version of the game you are playing (visible at the top left when the game menu is open, right below the game's title), and whether you have enabled cheats, and if so, which cheats.
