> As of v0.10.1

Increase your alchemy rank to learn new recipes. You can increase your rank by making any potion, but more complex or expensive potions grant more progress. Unranked recipes are learned during story events, from characters in the world, or as request rewards.

## Rank 1

- **Heat Liquor**: Increases lust. Effect is instant.
- **Preserved Sperm**: Sates hunger. Effect is instant.
- **Rancid Semen**: &hellip;? Surely it has some use&hellip;

## Rank 2

- **Moist Flask**: Makes the character *wet*. Effect is instant.
- **Calming Balm**: Reduces lust. Effect is instant.

## Rank 3

- **Love Potion**: Makes the target infatuated with the player character. Effect ends after the target orgasms.
- **Whore Liquor**: Makes the player character instantly orgasm whenever she so much as sees a hard penis. Temporary.

## Rank 4

- **Appeal Potion**: Raises the player's *seduction* skill. Temporary.
- **Magnetism Potion**: Raises the player's *charm* skill. Temporary.
- **Clever Potion**: Raises the player's *cunning* skill. Temporary.

## Rank 5

- **Merchant Salve**: Improves selling prices. Temporary.
- **Piss Liquor**: Makes the target have to urinate excessively. Temporary.

## Rank 6

- **Sensuous Perfume**: Increases the chances of random events occurring. Temporary.
- **Dirty Flask**: Makes the character *dirty*. Effect is instant.

## Rank 7

- **Dull Perfume**: Decreases the chances of random events occurring. Temporary.
- **Sleeping Potion**: Makes the target fall fast asleep. Effect is instant.

## Rank 8

- **Lactical**: Induces *lactation* in the target. Effect is permanent&mdash;take again to reverse the effect.
- **Succubus Snack**: Sates hunger. Stronger than <u>Preserved Sperm</u>. Effect is instant.

## Rank 9

- **Calming Balm +**: Reduces lust. Stronger than <u>Calming Balm</u>. Effect is instant.
- **Heat Liquor +**: Increases lust. Stronger than <u>Heat Liquor</u>. Effect is instant.

## Rank 10

- **Parity Potion**: Causes feeding in anyway to sate lust as well as hunger. Temporary.
- **Filthy Flask**: Makes the character *filthy*. Effect is instant.

## Rank 11

- **Appeal Potion +**: Raises the player's *seduction* skill. Stronger than <u>Appeal Potion</u>. Temporary.
- **Magnetism Potion +**: Raises the player's *charm* skill. Stronger than <u>Magnetism Potion</u>. Temporary.
- **Clever Potion +**: Raises the player's *cunning* skill. Stronger than <u>Clever Potion</u>. Temporary.

## Rank 12

- **Gross Flask**:  Makes the character *gross*. Effect is instant.
- **Cleaning Flask**:  Makes the character *clean*. Effect is instant.
- **Bottled Cock Stench**: Raises the player character's lust. Much stronger than a <u>Heat Liquor +</u>, but only works on the player character. Effect is instant.

## Rank 13

- **Semen Pearl**: Sates hunger a bit, but also increases lust. Has an "own brand" variant. Effect is instant.
- **Deviant Ale**: Raises lust but makes the player character immune to shame/embarrassment events. Temporary.

## Rank 14

- **Jizz Bomb**: Can be thrown at guards and NPCs. They will not be pleased. Has an "own brand" variant. Effect is instant.
- **Sloppy Lubricant**: Makes the player character's holes ooze with viscous white liquid, enhancing the sexual pleasure of partners. Temporary.

## Unranked

- **Futa Potion**: Causes the player character, or female NPCs, to grow a penis. Effect ends when the target orgasms.
- **Own Brand**: Preserved semen from the player's own "supply." Some potions also have "own brand" variants. Has no meaningful effect, unless&hellip;
- **Suggestion Potion**: Causes NPCs to obey the player character's commands for a short time. Temporary.
- **Charisma Potion**: Raises all the player's social skills at once. Temporary.

## Usable Items

These items are not potions, but can be found in the world and "used" via the inventory like potions. These cannot be crafted by the player.

- **Soiled Underwear**, **Damp Underwear**, **Messed Underwear**: Dirty male under garments. Use to increase the player character's lust. And maybe more&hellip;
- **Soiled Panties**, **Worn Panties**, **Pissed Panties**: Dirty female under garments. Use to increase the player character's lust. And maybe more&hellip;
