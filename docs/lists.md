# Lists

Lists of various in-game content: perks, recipes, the trait tree, and locations.

- [Traits](traits.md)
- [Perks](perks.md)
- [Potions](potions.md)
- [Locations](areas.md)
