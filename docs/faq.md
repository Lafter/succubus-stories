## FAQ

### How often will the game be updated?

~~I am aiming for *significant* monthly updates to the patron version. The free version will be updated less frequently, but I'll try to make sure most big content drops make it to the free version within a reasonable time frame. Bug fixes and patches will be released as I complete them, but probably not more than once per week&mdash;if I have a few patches I want to get out in the same week, unless one or more is critical and needs to go out ASAP, I'll probably bundle them together into a single patch. The monthly updates, unlike patches, will include content updates, including new scenes, new events, new locations, new progression, new side quests, and new story content. Patches will mostly be bug fixes and balance changes.~~

Update: The game will be updated on a roughly 6-8 week cycle. New updates will come to the patron edition first, and the free edition one week later. Specific update dates will always be announced on the Patreon page in an Update Preview post. Patches, hotfixes, and small updates will be released as appropriate to either or both versions.

### If I play a lot now, will I unlock tons of stuff in the next update right off the bat?

Because I think the progression is important to the game's pace, a small minority of progression elements will have a cap on them that increases slowly with each update. If it bothers you, you can use the cheat menu in the patron version to blow past these caps.

### Are you planning on adding more tiers to the Patreon campaign?

I am not planning to but I am certainly *willing* to. If you want to enter at a different price point than what's on offer, or if you can think of a cool bonus you'd like that isn't available, feel free to create an issue here or reach out to me over on Discord, the Patreon, or my personal website and let me know what you're thinking.

### Can I use the game's code or assets for my project?

For the music, sound effects, icons, and third-party software, refer to their licenses (most are permissive). Regarding the art assets (that is, the renders) and code of *Succubus Stories* itself, no, all that content is intended to be closed source and all rights are reserved. 

That said, feel free to reach out to me, I'm open to providing the required permissions to individuals on a case by case basis.

### Can I mod or otherwise alter the game?

Official mod support is planned, in addition to support for custom art and sound packs. I would encourage users to wait for that. Even people familiar with Twine, web development, and JavaScript are not necessarily well equipped to work on *Succubus Stories* without breaking things.

### Are there plans for more than just straight sex?

~~Note that there is a very small amount of lesbian/bisexual content in the game, and while more will be added in the future, this type of content will continue to be a relatively small portion of the game's content.~~

~~I'm focusing on nailing down the straight female-on-male experience for right now as that forms what I consider to be the core of the succubus theme. I have loose and not set-in-stone plans to add both lesbian sex and futanari mechanics as official mods. Mod support also means users could also potentially add even more options at that time.~~

~~However, the game will remain mostly straight for the foreseeable future, so I can't recommend becoming a patron or anything if the straight stuff doesn't do it for you. Mod tools and official mods are a very, very long way off. If you otherwise like the mechanics, check back every so often and see where we're at.~~

The game now features a large amount of lesbian and futanari content!